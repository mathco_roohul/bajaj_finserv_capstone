import pyodbc
import pandas as pd
import datetime
import connection
import create_and_drop

#Insert data into enquiry_fin table
def insert_enq_fin_table(df):
    cursor, cnxn = connection.connect()
    date = datetime.datetime.today()
    insert_enquiry_fin = """
                        INSERT INTO finserv.enquiry_fin([ENQUIRY], [DEALER_CITY], [BRANCH_TYPE], [LEAD_TYPE], [STATE], [CITY], [MODEL_FAMILY], [MODEL_CODE], [TOTAL_AMOUNT], [TEST_RIDE_OFFERED], [FOLLOW_UP], [SRC_OF_ENQ], [UPDATE_DATE])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index,row in df.iterrows():
        cursor.execute(insert_enquiry_fin, row['Enquiry'], row['DealerCity'], row['BranchType'], row['LeadType'], row['State'], row['City'], row['ModelFamily'], row['ModelCode'], row['TotalAmount'], row['TestRideOffered'], row['FollowUp'], row['SourceOfEnquiry'], date)
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)

#Insert data into enquiry_stg table
def insert_enq_stg_table(df):
    cursor, cnxn = connection.connect()
    create_and_drop.drop_enq_stg_table()
    create_and_drop.create_enq_stg_table()
    insert_enquiry_stg = """
                        INSERT INTO finserv.enquiry_stg([ENQUIRY], [DEALER_CITY], [BRANCH_TYPE], [LEAD_TYPE], [STATE], [CITY], [MODEL_FAMILY], [MODEL_CODE], [TOTAL_AMOUNT], [TEST_RIDE_OFFERED], [FOLLOW_UP], [SRC_OF_ENQ]) 
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
    """
    i=0
    for index, row in df.iterrows():
        cursor.execute(insert_enquiry_stg, row['Enquiry'], row['DealerCity'], row['BranchType'], row['LeadType'], row['State'], row['City'], row['ModelFamily'], row['ModelCode'], row['TotalAmount'], row['TestRideOffered'], row['FollowUp'], row['SourceOfEnquiry'])
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)

#Insert data into retail_fin table
def insert_ret_fin_table(df):
    cursor, cnxn = connection.connect()
    date = datetime.datetime.today()
    insert_ret_fin = """
                        INSERT INTO finserv.retail_fin([ENQUIRY], [STATE], [INVOICE_NO], [INVOICE_DATE], [DEALER_CITY], [BRANCH_TYPE], [CUSTOMER_CODE], [CITY], [MODEL_FAMILY], [MODEL_CODE], [COLOR], [CHASSIS_NO], [TOTAL_AMOUNT], [SEGMENT], [ENQUIRY_FLAG], [BOOKING_FLAG], [UPDATE_DATE])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index,row in df.iterrows():
        cursor.execute(insert_ret_fin, row['Enquiry'], row['State'], row['InvoiceNo'], row['InvoiceDate'], row['DealerCity'], row['BranchType'], row['CustomerCode'], row['City'], row['ModelFamily'], row['ModelCode'], row['Color'], row['ChassisNo'], row['TotalAmount'], row['Segment'], row['Enquiry_flag'], row['Booking_flag'], date)
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)

#Insert data into retail_stg table
def insert_ret_stg_table(df):
    cursor, cnxn = connection.connect()
    create_and_drop.drop_ret_stg_table()
    create_and_drop.create_ret_stg_table()
    insert_ret_stg = """
                        INSERT INTO finserv.retail_stg([ENQUIRY], [STATE], [INVOICE_NO], [INVOICE_DATE], [DEALER_CITY], [BRANCH_TYPE], [CUSTOMER_CODE], [CITY], [MODEL_FAMILY], [MODEL_CODE], [COLOR], [CHASSIS_NO], [TOTAL_AMOUNT], [SEGMENT], [ENQUIRY_FLAG], [BOOKING_FLAG])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index, row in df.iterrows():
        cursor.execute(insert_ret_stg, row['Enquiry'], row['State'], row['InvoiceNo'], row['InvoiceDate'], row['DealerCity'], row['BranchType'], row['CustomerCode'], row['City'], row['ModelFamily'], row['ModelCode'], row['Color'], row['ChassisNo'], row['TotalAmount'], row['Segment'], row['Enquiry_flag'], row['Booking_flag'])
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)


#Insert data into merged_fin table
def insert_merged_fin_table(df):
    cursor, cnxn = connection.connect()
    date = datetime.datetime.today()
    insert_merged_fin = """
                        INSERT INTO finserv.merged_fin([ENQUIRY], [STATE], [MODEL_CODE], [CHASSIS_NO], [INVOICE_DATE], [COLOR], [SEGMENT], [ENQUIRY_FLAG], [BOOKING_FLAG], [CUSTOMER_CODE], [INVOICE_NO], [LEAD_TYPE], [TEST_RIDE_OFFERED], [FOLLOW_UP], [SRC_OF_ENQ], [BRANCH_TYPE], [CITY], [DEALER_CITY], [TOTAL_AMOUNT], [MODEL_FAMILY], [UPDATE_DATE])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index,row in df.iterrows():
        cursor.execute(insert_merged_fin,row['Enquiry'], row['State'], row['ModelCode'], row['ChassisNo'], row['InvoiceDate'], row['Color'], row['Segment'], row['Enquiry_flag'], row['Booking_flag'], row['CustomerCode'], row['InvoiceNo'], row['LeadType'], row['TestRideOffered'], row['FollowUp'], row['SourceOfEnquiry'], row['BranchType'], row['City'], row['DealerCity'], row['TotalAmount'], row['ModelFamily'], date)
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)

#Insert data into merged_stg table
def insert_merged_stg_table(df):
    cursor, cnxn = connection.connect()
    create_and_drop.drop_merged_stg_table()
    create_and_drop.create_merged_stg_table()
    insert_merged_stg = """
                        INSERT INTO finserv.merged_stg([ENQUIRY], [STATE], [MODEL_CODE], [CHASSIS_NO], [INVOICE_DATE], [COLOR], [SEGMENT], [ENQUIRY_FLAG], [BOOKING_FLAG], [CUSTOMER_CODE], [INVOICE_NO], [LEAD_TYPE], [TEST_RIDE_OFFERED], [FOLLOW_UP], [SRC_OF_ENQ], [BRANCH_TYPE], [CITY], [DEALER_CITY], [TOTAL_AMOUNT], [MODEL_FAMILY])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index, row in df.iterrows():
        cursor.execute(insert_merged_stg, row['Enquiry'], row['State'], row['ModelCode'], row['ChassisNo'], row['InvoiceDate'], row['Color'], row['Segment'], row['Enquiry_flag'], row['Booking_flag'], row['CustomerCode'], row['InvoiceNo'], row['LeadType'], row['TestRideOffered'], row['FollowUp'], row['SourceOfEnquiry'], row['BranchType'], row['City'], row['DealerCity'], row['TotalAmount'], row['ModelFamily'])
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)

#Insert data into pred_fin table
def insert_pred_fin_table(df):
    cursor, cnxn = connection.connect()
    date = datetime.datetime.today()
    insert_pred_fin = """
                        INSERT INTO finserv.predicted_fin([ENQUIRY], [STATE], [MODEL_CODE], [CHASSIS_NO], [INVOICE_DATE], [COLOR], [SEGMENT], [ENQUIRY_FLAG], [BOOKING_FLAG], [CUSTOMER_CODE], [INVOICE_NO], [LEAD_TYPE], [TEST_RIDE_OFFERED], [FOLLOW_UP], [SRC_OF_ENQ], [BRANCH_TYPE], [CITY], [DEALER_CITY], [TOTAL_AMOUNT], [MODEL_FAMILY], [LOAN_TAKEN_PREDICT], [LOAN_TAKEN_PROBABILITY], [LOAN_TAKEN_TAG], [UPDATE_DATE])
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)                            
    """
    i=0
    for index,row in df.iterrows():
        cursor.execute(insert_pred_fin,row['Enquiry'], row['State'], row['ModelCode'], row['ChassisNo'], row['InvoiceDate'], row['Color'], row['Segment'], row['Enquiry_flag'], row['Booking_flag'], row['CustomerCode'], row['InvoiceNo'], row['LeadType'], row['TestRideOffered'], row['FollowUp'], row['SourceOfEnquiry'], row['BranchType'], row['City'], row['DealerCity'], row['TotalAmount'], row['ModelFamily'], row['LoanTakenPredict'], ['LoanTakenProbability'], ['LoanTakenTag'], date)
        i+=1
    cursor.commit()
    print("Success: {} rows inserted".format(i))
    connection.close_conn(cnxn)
