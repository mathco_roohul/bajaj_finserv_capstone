import pandas as pd
import create_and_drop
import insertdata
import getdata

print("Starting inserting enquiry data")
enq_df = pd.read_csv(r"D:\CapstoneProject\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 3\Cleaned_data\enquiry_df_cleaned.csv")
enq_df = enq_df.sample(frac = 0.05)
# insertdata.insert_enq_fin_table(enq_df)
# insertdata.insert_enq_stg_table(enq_df)
print("End inserting enquiry data")

print("Starting inserting retail data")
ret_df = pd.read_csv(r"D:\CapstoneProject\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 3\Cleaned_data\retail_df_cleaned.csv")
ret_df = ret_df.sample(frac = 0.05)
insertdata.insert_ret_fin_table(ret_df)
# insertdata.insert_ret_stg_table(ret_df)
print("End inserting retail data")

print("Starting inserting merged data")
merged_df = pd.read_csv(r"D:\CapstoneProject\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 3\Merged_data\merged_df.csv")
merged_df = merged_df.sample(frac = 0.05)
insertdata.insert_merged_fin_table(merged_df)
# insertdata.insert_merged_stg_table(merged_df)
print("End inserting merged data")

# ret_df = getdata.get_ret_stg_data()
# print(ret_df.head(10))
# print(ret_df.columns)
# print(type(ret_df))
