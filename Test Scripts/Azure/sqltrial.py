import pyodbc
import pandas as pd
from sqlalchemy import event
import sqlalchemy 
import urllib
import pymysql
# insert data from csv file into dataframe.
# working directory for csv file: type "pwd" in Azure Data Studio or Linux
# working directory in Windows c:\users\username
#df = pd.read_csv(r"Daily_work\New DataSet\Original Data\Enquiry_data.csv")
data = [[2,'a'],[3,'b']]
df = pd.DataFrame(data, columns=['id', 'name'])
# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance

# server = 'capstonesqlnew.database.windows.net,1433' # to specify an alternate port
# # server = 'yourservername' 
# database = 'Capstone_AzureSQL' 
# username = 'Mathco' 
# password = 'Math@1234$' 
# cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
# cursor = cnxn.cursor()

# #Create table
# cursor.execute("INSERT INTO finserv.dummy2 values(1, 'n')")

# # Insert Dataframe into SQL Server:
# # for index, row in df.iterrows():
# #     cursor.execute("INSERT INTO HumanResources.DepartmentTest (DepartmentID,Name,GroupName) values(?,?,?)", row.DepartmentID, row.Name, row.GroupName)
# cnxn.commit()
# cursor.close()
# # print(df.head())

server='capstonesqlnew.database.windows.net'
host='capstonesqlnew.database.windows.net'
port = 1433
database= 'Capstone_AzureSQL'
username='Mathco'
user='Mathco'
password='Math@1234$'
passw='Math@1234$'
driver='{SQL Server}'
params= 'Driver='+driver + ';SERVER='+server +';PORT=1433;DATABASE=' +database + ';UID=' +username +';PWD=' +password
db_params= urllib.parse.quote_plus(params)
engine= sqlalchemy.create_engine("mysql+pyodbc:///?odbc_connect={}".format(db_params))
# engine = sqlalchemy.create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database , echo=False)# @event.listens_for(engine,"before_cursor_execute")
# def receive_before_cursor_execute(conn,cursor,statement,params,context,executemany):
#     if executemany:
#         cursor.fast_executemany=True
table = "dummy2"
# df.to_sql(table,engine,index=True, if_exists="append",schema="finserv")
# df1 = pd.DataFrame()
# query = 'SELECT * FROM dummy2'
# df1 = pd.read_sql(query, engine)
# print(df1)
with engine.connect() as connection:
    result = connection.execute("select name from dummy2")
    for row in result:
        print("name:", row['name'])