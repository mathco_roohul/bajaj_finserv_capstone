# import required libraries
import pandas as pd
import numpy as np

# set datafile paths
retail_df_path = "D:/MathCo/Capstone Project/GitLab/bajaj_finserv_capstone/Daily_work/New DataSet/Original Data/Enquiry_data.csv"
enquiry_df_path = "D:/MathCo/Capstone Project/GitLab/bajaj_finserv_capstone/Daily_work/New DataSet/Original Data/Retail_data.csv"

# read datasets
retail_df = pd.read_csv(retail_df_path)
enquiry_df = pd.read_csv(enquiry_df_path)

# preprocessing oof city names
enquiry_df['DealerCity'] = enquiry_df['DealerCity'].str.replace("COCHIN","KOCHI").tolist()
retail_df['DealerCity'] = retail_df['DealerCity'].str.replace("COCHIN","KOCHI").tolist()

# extract city-branch from enquiry_df
enq_city_branch = sorted(enquiry_df[['DealerCity','BranchType']].groupby(['DealerCity','BranchType']).size().to_dict().keys())
city_branch = dict()
for city,branch in enq_city_branch:
    city_branch[city.title()] = branch

# extract city-branch from retail_df
ret_city_branch = sorted(retail_df[['DealerCity','BranchType']].groupby(['DealerCity','BranchType']).size().to_dict().keys())
for city,branch in ret_city_branch:
    if city in city_branch.keys(): 
        pass
    else: 
        city_branch[city.title()] = branch

# create a dataframe of city-branch
city_branch_df = pd.DataFrame(data={'DealerCity':list(city_branch.keys()),'BranchType':list(city_branch.values())})

# save the dataframe
city_branch_df.to_csv("city_branch_df.csv",index=False)

