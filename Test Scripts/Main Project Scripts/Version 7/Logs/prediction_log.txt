Model Name: RandomForestClassifier
Model Parameters:
{
  "bootstrap": false,
  "ccp_alpha": 0.0,
  "class_weight": null,
  "criterion": "gini",
  "max_depth": null,
  "max_features": 0.2,
  "max_leaf_nodes": null,
  "max_samples": null,
  "min_impurity_decrease": 0.0,
  "min_impurity_split": null,
  "min_samples_leaf": 4,
  "min_samples_split": 2,
  "min_weight_fraction_leaf": 0.0,
  "n_estimators": 100,
  "n_jobs": null,
  "oob_score": false,
  "random_state": null,
  "verbose": 0,
  "warm_start": false
}

Performance Metrics:
Accuracy: 0.5169938116418488
Precision: 0.3473372781065089
Recall: 0.7193039784331345
F1_Score: 0.46846319597776065

Features and their contribution to prediction:
{
  "TotalAmount": "35.05%",
  "State": "18.43%",
  "FollowUp": "12.79%",
  "LeadType": "10.1%",
  "SourceOfEnquiry": "7.41%",
  "TestRideOffered": "6.8%",
  "ModelFamily": "2.55%",
  "City": "2.14%",
  "DealerCity": "1.98%",
  "Segment": "1.78%",
  "BranchType": "0.97%",
  "Enquiry_flag": "0.0%",
  "Booking_flag": "0.0%"
}