# Import necessary libraries
import numpy as np
import pandas as pd
import json
import sys
import os
from datetime import datetime

# code to ignore all warnings
import warnings
warnings.filterwarnings('ignore')

class FileCheck:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            with open('init.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
        except FileNotFoundError:
            print("Error: The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("Error: The passed filename or filetype is improper, please check the code and rectify it")
        except Exception as e:
            print("Error: The code stopped because of an error. The error is {}".format(e))
        
# ----------------------------------------------------------------------------------------------------------------------

    # check if retail_df is proper
    def check_retail_data(self, retail_df):

        # read the data
        self.retail_df = retail_df

        # check if all the required columns are present in the data file
        try:
            self.retail_columns = pd.Series(self.init['original_data']["retail_df"]['columns'])
            if self.retail_columns.isin(self.retail_df.columns).all():pass
            else:
                e = "The data selected does not have the required columns"
                self.log_errors(error=e,function_name="check_retail_data")
                return False
        except Exception as e:
            self.log_errors(error=e,function_name="check_retail_data")
            return False

        # return True as there are no problems with the data
        return True

# ----------------------------------------------------------------------------------------------------------------------

    # check if enquiry_df is proper
    def check_enquiry_data(self, enquiry_df):

        # read the data
        self.enquiry_df = enquiry_df

        # check if all the required columns are present in the data file
        try:
            self.enquiry_columns = pd.Series(self.init['original_data']["enquiry_df"]['columns'])
            if self.enquiry_columns.isin(self.enquiry_df.columns).all():pass
            else:
                e = "The data selected does not have the required columns"
                self.log_errors(error=e,function_name="check_enquiry_data")
                return False
        except Exception as e:
            self.log_errors(error=e,function_name="check_enquiry_data")
            return False

        # return True as there are no problems with the data
        return True

# ----------------------------------------------------------------------------------------------------------------------
    
    # check if finance_df is proper
    def check_finance_data(self, finance_df):

        # read the data
        self.finance_df = finance_df

        # check if all the required columns are present in the data file
        try:
            self.finance_columns = pd.Series(self.init['original_data']["finance_df"]['columns'])
            if self.finance_columns.isin(self.finance_df.columns).all():pass
            else:
                e = "The data selected does not have the required columns"
                self.log_errors(error=e,function_name="check_finance_data")
                return False
        except Exception as e:
            self.log_errors(error=e,function_name="check_finance_data")
            return False

        # return True as there are no problems with the data
        return True

# ----------------------------------------------------------------------------------------------------------------------   
            

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="FileCheck", script_name="FileCheck.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: {}\n\n".format(error))


# ----------------------------------------------------------------------------------------------------------------------   

# Code Dump
# # check if all column names are in title format
#         try:
#             if self.enquiry_df.columns.str.istitle().all() != True:
#                 e = "The column names are not in title format"
#                 self.log_errors(error=e,function_name="check_enquiry_data")
#                 return False
#         except Exception as e:
#             self.log_errors(error=e,function_name="check_enquiry_data")
#             return False




    