import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from FileCheck import FileCheck
from BlobConnect import Blob
import json
import io

with open('init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']

#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="FileCheck", script_name="check_retail.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))

#--Extract Retail.csv from blob---------------------------------------------
try:
    output = io.StringIO()
    blob = Blob()
    retail_df = blob.download_data("Sample_data/Retail_data/20201012_Retail_data_sample.csv")
except Exception as e:
    a = "\tLocation: Reading retail data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#--Extract enquiry.csv from blob---------------------------------------------
try:
    output = io.StringIO()
    blob = Blob()
    enquiry_df = blob.download_data("Sample_data/Enquiry_data/20201012_Enquiry_data_sample.csv")
except Exception as e:
    a = "\tLocation: Reading enquiry data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#--Check if the retail df is proper------------------------------------------
try:
    check_retail = FileCheck()
    if check_retail.check_retail_data(retail_df) == True:
        print("Retail data is good")
    else: 
        print("Retail data is not as expected")
except Exception as e:
    a = "\tLocation: Checking Retail data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="check_retail_data")

#--Check if the enquiry df is proper------------------------------------------
try:
    check_enquiry = FileCheck()
    if check_enquiry.check_enquiry_data(enquiry_df) == True:
        print("Enquiry data is good")
    else: 
        print("Enquiry data is not as expected")
except Exception as e:
    a = "\tLocation: Checking enquiry data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="check_enquiry_data")


