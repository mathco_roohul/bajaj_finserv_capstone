import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from BlobConnect import Blob
import json
import io

with open('init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']
#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="Clean", script_name="t_upload_clean_enquiry_data.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))

#--Extract cleaned retail.csv from blob---------------------------------------------

try:
    output = io.StringIO()
    blob = Blob()
    cleaned_enquiry_df = blob.download_data("Cleaned_data/Enquiry_data/20201012_Enquiry_data_cleaned.csv")
except Exception as e:
    a = "\tLocation: Reading enquiry data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#--Pushing data into SQL database-------------------------------------------
#Pushing data into SQL staging table
try:
    insert_data_to_db.insert_enq_stg_table(cleaned_enquiry_df)
    print("Inserted cleaned enquiry data into staging table successfully.")
except Exception as e:
    a = "\tLocation: enquiry Staging Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_enq_stg_table")

#Pushing data into SQL final table
try:
    insert_data_to_db.insert_enq_fin_table(cleaned_enquiry_df)
    print("Inserted cleaned enquiry data into final table successfully.")
except Exception as e:
    a = "\tLocation: enquiry Final Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_enq_fin_table")
