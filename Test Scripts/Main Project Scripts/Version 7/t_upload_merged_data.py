import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from BlobConnect import Blob
import json
import io

with open('init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']
#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="Merge", script_name="t_upload_merged_data.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))

#--Extract merged data from blob---------------------------------------------

try:
    output = io.StringIO()
    blob = Blob()
    merged_df = blob.download_data("Merged_data/Retail_Enquiry/20201012_Merged_data.csv")
except Exception as e:
    a = "\tLocation: Reading merged data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#--Pushing merged data into SQL merged table-------------------------------------------------

#Pushing data into SQL staging table
try:
    insert_data_to_db.insert_merged_stg_table(merged_df)
    print("Inserted cleaned merged data into staging table successfully.")
except Exception as e:
    a = "\tLocation: Merged Staging Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_merged_stg_table")

#Pushing data into SQL final table
try:
    insert_data_to_db.insert_merged_fin_table(merged_df)
    print("Inserted cleaned retail data into final table successfully.")
except Exception as e:
    a = "\tLocation: Merged Final Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_merged_fin_table")