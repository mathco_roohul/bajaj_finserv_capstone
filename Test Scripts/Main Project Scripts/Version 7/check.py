from FileCheck import FileCheck
from CleanData import Clean
from MergeData import Merge
from DivideData import Divide
from PredictData import Predict

check = FileCheck()
print(check.check_finance_data())
clean = Clean()
print(clean.clean_finance_data())
merge = Merge()
print(merge.merge_retail_enquiry())
# print(merge.merge_finance())
divide = Divide()
print(divide.divide_data())
predict = Predict()
print(predict.predict_data())
