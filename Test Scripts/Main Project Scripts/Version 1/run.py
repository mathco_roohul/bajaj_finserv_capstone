# import required libraries
import pandas as pd
import numpy as np

# import project scripts
import clean_data
import merge_data
import divide_data
import train_data
import predict_data

# clean the datasets
print("Cleaning started")
clean_data.clean(filename="retail_df")
print("Retail Dataset Cleaned")
clean_data.clean(filename="enquiry_df")
print("Enquiry Dataset Cleaned")
clean_data.clean(filename="finance_df")
print("Finance Dataset Cleaned")

# merge the datasets
print("Merging started")
merge_data.merge()
print("Retail, Finance and Enquiry Datasets merged")

# divide the data into main data and new data
print("Data division started")
divide_data.divide(percentage=0.8)
print("Data divided into main and new datasets")

# train the data and save models
print("Training started")
train_data.train_evaluate()
print("Training completed, check the log file and models folders")

# predict the output for new data
print("Prediction started")
predict_data.predict()
print("Data prediction completed")

# print the output
# print(main_df.shape, new_df.shape)
