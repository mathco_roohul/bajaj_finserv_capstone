'''Imports'''

# import required dataframes
import json
import pandas as pd
import numpy as np
from datetime import datetime

# import undersampling libraries
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import NeighbourhoodCleaningRule
from imblearn.under_sampling import RandomUnderSampler


# import oversampling libraries
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.over_sampling import ADASYN
from imblearn.pipeline import Pipeline

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import MinMaxScaler

# import ML models
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import make_pipeline, make_union
from sklearn.tree import DecisionTreeClassifier
from tpot.builtins import StackingEstimator
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold

# import model evaluation library
from sklearn.metrics import SCORERS

# import model saving library
import pickle


# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)


'''Initializations - Variables'''

# create initial variables
count = 0
invoice_city_dict = dict()
invoice_dlcity_dict = dict()
model_scores_dict = dict()
model_scores_list = list()
in_model_dict = dict()
out_model_dict = dict()
encoder_dict = dict()
sampler_dict = dict()


'''Initialization - Lists'''

classification_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'TotalAmount', 'City', 'State', 'DealerCity', 'ModelFamily']
label_encode_cols = ['Segment', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
standardize_cols = ['TotalAmount']
# List down Tier 1 and 2 companies
Tier1 = ['Bengaluru','Chennai','Delhi','Hyderabad','Kolkata','Mumbai','Ahmedabad','Pune']
Tier2 = ['Agra','Aligarh','Amravati','Asansol','Bareilly','Bhavnagar','Bhopal','Bikaner','Bokaro Steel City',
         'Coimbatore','Dehradun','Bhilai','Erode','Firozabad','Gorakhpur','Guntur','Gurgaon','Hubli','Dharwad',
         'Indore','Jaipur','Jammu','Jamshedpur','Jodhpur','Kannur','Kochi','Kolhapur','Kota','Kurnool',
         'Lucknow','Malappuram','Goa','Meerut','Mysore','Nanded','Nellore','Palakkad','Perinthalmanna','Purulia',
         'Rajkot','Ranchi','Salem','Shimla','Solapur','Thiruvananthapuram','Tiruchirappalli','Tirupati','Tiruppur',
         'Ujjain','Vadodara','Vasai','Virar City','Vellore','Surat','Ajmer','Allahabad','Amritsar','Aurangabad',
         'Belgaum','Bhiwandi','Bhubaneswar','Bilaspur','Chandigarh','Cuttack','Dhanbad','Durgapur','Faridabad',
         'Ghaziabad','Gulbarga','Gwalior','Guwahati','Hamirpur','Jabalpur','Jalandhar','Jamnagar','Jhansi','Kakinada',
         'Kanpur','Kottayam','Kollam','Kozhikode','Ludhiana','Madurai','Mathura','Mangalore','Moradabad','Nagpur',
         'Nashik','Noida','Patna','Pondicherry','Raipur','Rajahmundry','Rourkela','Sangli','Siliguri','Srinagar',
         'Thrissur','Tirur','Tirunelveli','Tiruvannamalai','Bijapur','Varanasi','Vijayawada','Warangal','Visakhapatnam']


'''Initializations - Samplers'''

# populate the dict with undersampling functions
sampler_dict['US_ENN'] = EditedNearestNeighbours()
sampler_dict['US_RU'] = RandomUnderSampler()
sampler_dict['US_NM'] = NearMiss(version=3)
sampler_dict['US_TL'] = TomekLinks()
sampler_dict['US_OSS'] = OneSidedSelection()
sampler_dict['US_NCR'] = NeighbourhoodCleaningRule(n_neighbors=3, threshold_cleaning=0.5)


# populate the dict with oversampling functions
sampler_dict['OS_SM'] = SMOTE()
sampler_dict['OS_BSM'] = BorderlineSMOTE()
sampler_dict['OS_ASM'] = ADASYN()


# # populate the dict with pipeline functions
# under = EditedNearestNeighbours(sampling_strategy=1)

# # pipeline 1
# over1 = SMOTE(sampling_strategy=0.5)
# steps1 = [('o', over1), ('u', under)]
# sampler_dict['PL_SUS'] = Pipeline(steps=steps1)

# # pipeline 2
# over2 = BorderlineSMOTE(sampling_strategy=0.5)
# steps2 = [('o', over2), ('u', under)]
# sampler_dict['PL_BUS'] = Pipeline(steps=steps2)

# # pipeline 3
# over3 = ADASYN(sampling_strategy=0.5)
# steps3 = [('o', over3), ('u', under)]
# sampler_dict['PL_SUS'] = Pipeline(steps=steps3)


'''Initializations - Models'''

# populate the in_model_dict with model definitions
in_model_dict['KNN_1'] = KNeighborsClassifier(n_neighbors=62, p=1, weights="distance")
in_model_dict['RF_1'] = RandomForestClassifier(bootstrap=False, criterion="gini", max_features=0.2, min_samples_leaf=4, min_samples_split=2, n_estimators=100)
in_model_dict['RF_2'] = RandomForestClassifier(bootstrap=False, criterion="gini", max_features=0.15000000000000002, min_samples_leaf=2, min_samples_split=13, n_estimators=100)
in_model_dict['RF_3'] = RandomForestClassifier()
in_model_dict['DT_1'] = DecisionTreeClassifier()


# populate the in_model_dict with pipeline definitions
# pipeline 1
exported_pipeline1 = make_pipeline(
    PCA(iterated_power=3, svd_solver="randomized"),
    StackingEstimator(estimator=DecisionTreeClassifier(criterion="entropy", max_depth=1, min_samples_leaf=17, min_samples_split=11)),
    KNeighborsClassifier(n_neighbors=2, p=2, weights="distance")
)
in_model_dict['PL_1'] = exported_pipeline1

# pipeline 2
exported_pipeline2 = make_pipeline(    
    StackingEstimator(estimator=DecisionTreeClassifier(criterion="gini", max_depth=8, min_samples_leaf=19, min_samples_split=3)),    
    VarianceThreshold(threshold=0.0005),    
    PCA(iterated_power=10, svd_solver="randomized"),    
    PCA(iterated_power=5, svd_solver="randomized"),    
    KNeighborsClassifier(n_neighbors=96, p=1, weights="distance"))
in_model_dict['PL_2'] = exported_pipeline2


'''Function definitions'''

# function to create invoice-city pair dictionary
def make_invoice_city_dict(df,city_col="City"):
    # create a dict of invoice number and city
    inv_df = df.copy()
    inv_df = inv_df.set_index('InvoiceNo')
    invoice_city_dict = inv_df.to_dict()[city_col]    
    
    # return the dictionary
    return invoice_city_dict


# function to convert the city names to their respective tiers
def city_tiers(x):
    x = x.title()
    if x in Tier1: return "Tier1"
    elif x in Tier2: return "Tier2"
    else: return 'Tier3'
    
    
# function to evaluate the model
def evaluate_model(sampling_type, model_name, Xtrain=[], ytrain=[], Xtest=[], ytest=[]):
    # train the model
    model = in_model_dict[model_name]
    model.fit(Xtrain, ytrain)
    out_model_dict[sampling_type+"_"+model_name] = model
    
    # fill the scores in the dictionary
    performance_dict = dict()
    performance_dict['Sampling'] = sampling_type
    performance_dict['Model'] = model_name

    # evaluate the model for training data
    if (len(Xtrain) > 0) and (len(ytrain) > 0):
        performance_dict['accuracy_train'] = SCORERS['accuracy'](model, Xtrain, ytrain)
        performance_dict['precision_train'] = SCORERS['precision'](model, Xtrain, ytrain)
        performance_dict['recall_train'] = SCORERS['recall'](model, Xtrain, ytrain)
        performance_dict['f1_train'] = SCORERS['f1'](model, Xtrain, ytrain)
        
    # evaluate the model for testing data
    if (len(Xtest) > 0) and (len(ytest) > 0):
        performance_dict['accuracy_test'] = SCORERS['accuracy'](model, Xtest, ytest)
        performance_dict['precision_test'] = SCORERS['precision'](model, Xtest, ytest)
        performance_dict['recall_test'] = SCORERS['recall'](model, Xtest, ytest)
        performance_dict['f1_test'] = SCORERS['f1'](model, Xtest, ytest)
        
    # save the model
    path_to_save_model = init["all_models"]["path"]
    now = datetime.now()
    dt_string = now.strftime("%Y_%m_%d_%H_%M_%S")
    filename = dt_string+"_"+model_name+"_"+sampling_type+"_f1score_"+str(performance_dict['f1_test'])+".sav"
    pickle.dump(model, open(path_to_save_model+filename, 'wb'))

    # return the scores
    return performance_dict



'''Main training and evaluation function'''

# function to classify rows in dataframe
def train_evaluate():

    # read the data to train the model
    df = pd.read_csv(init['divided_data']["main_df"]['path'])
    
    # # create a dict of invoice number and city
    # invoice_city_dict = make_invoice_city_dict(df,city_col='City')
    # invoice_dlcity_dict = make_invoice_city_dict(df,city_col='DealerCity')
    
    
    # convert cities into tiers
    df['City'] = df['City'].apply(city_tiers)
    df['DealerCity'] = df['DealerCity'].apply(city_tiers)
    
    
    # label encode the required columns
    for col in label_encode_cols:
        label_encoder = LabelEncoder()
        encoder_dict[col] = label_encoder.fit(df[col])
        df[col] = encoder_dict[col].transform(df[col])
       
    
    # standardize the required columns
    for col in standardize_cols:
        mmscaler = MinMaxScaler()
        encoder_dict[col] = mmscaler.fit(df[col].values.reshape(-1, 1))
        df[col] = encoder_dict[col].transform(df[col].values.reshape(-1, 1))
    
    
    # Set X and y for training
    y = df['LoanTaken']
    X = df.drop('LoanTaken',axis=1)
    
    
    # sample the data and classify it
    for sampling in sampler_dict.keys():
        
        
        # sample the dataset
        X_s, y_s = sampler_dict[sampling].fit_resample(X[classification_cols],y)
        
        
        # split the data in equal proportions of 1s and 0s
        data_split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=100)
        for train_index, test_index in data_split.split(X_s, y_s):
            Xtrain, Xtest = X_s.iloc[train_index], X_s.iloc[test_index]
            ytrain, ytest = y_s.iloc[train_index], y_s.iloc[test_index]
           
        
        # Model evaluation loop
        for model_name in in_model_dict.keys():
            global count
            count += 1
            
            
            # train and evaluate the model
            model_scores_dict['Model_{}'.format(count)] = evaluate_model(sampling_type=sampling, model_name=model_name, Xtrain=Xtrain, ytrain=ytrain, Xtest=Xtest[classification_cols], ytest=ytest)
            
            
            # log the performance scores
            model_log_path = init['modelling_log']['path']
            with open(model_log_path,'a+') as modelling_log_file:
                modelling_log_file.write(json.dumps(model_scores_dict['Model_{}'.format(count)])+"\n")
            
            
            # store dictionary of each iteration in a list to make dataframe
            model_scores_list.append(model_scores_dict['Model_{}'.format(count)])
            
            
    # create a dataframe of performance metrics
    performance_metrics_df = pd.DataFrame(model_scores_list, columns=['Sampling', 'Model', 'accuracy_train', 'precision_train', 'recall_train', 'f1_train', 'accuracy_test', 'precision_test', 'recall_test', 'f1_test'])
    
    
    # sort the dataframe in descending order with respect to f1_test
    performance_metrics_df = performance_metrics_df.sort_values(['f1_test'],ascending=[False])
    
    
    # # choose the best classifier
    # top_model_name = performance_metrics_df['Sampling'][0] + '_' + performance_metrics_df['Model'][0]
    # top_model = out_model_dict[top_model_name]
    # print("Best model = {} with F1 score = {}".format(top_model_name, performance_metrics_df['f1_test'][0]))
    
    
    # # save the best model
    # path_to_save_model = init["best_model"]["path"]
    # now = datetime.now()
    # dt_string = now.strftime("%Y_%m_%d_%H_%M_%S")
    # filename = dt_string+"_"+top_model_name+"_f1score_"+str(performance_metrics_df['f1_test'][0])+".sav"
    # pickle.dump(top_model, open(path_to_save_model+'_'+filename, 'wb'))
    
    
    # # return Performance metrics dataframe and best model
    # return (performance_metrics_df, top_model)

    # save the performance metrics df
    performance_metrics_df.to_csv(init['performance_data']["performance_metrics_df"]['path'],index=False)

