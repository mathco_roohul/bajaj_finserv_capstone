# imort required libraries
import pandas as pd
import numpy as np
import json
import pickle
import os

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

# import model evaluation library
from sklearn.metrics import SCORERS


# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)


'''Initializations - Variables'''

# create initial variables
count = 0
invoice_city_dict = dict()
invoice_dlcity_dict = dict()
encoder_dict = dict()


'''Initialization - Lists'''

classification_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'TotalAmount', 'City', 'State', 'DealerCity', 'ModelFamily']
label_encode_cols = ['Segment', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
standardize_cols = ['TotalAmount']
# List down Tier 1 and 2 companies
Tier1 = ['Bengaluru','Chennai','Delhi','Hyderabad','Kolkata','Mumbai','Ahmedabad','Pune']
Tier2 = ['Agra','Aligarh','Amravati','Asansol','Bareilly','Bhavnagar','Bhopal','Bikaner','Bokaro Steel City',
         'Coimbatore','Dehradun','Bhilai','Erode','Firozabad','Gorakhpur','Guntur','Gurgaon','Hubli','Dharwad',
         'Indore','Jaipur','Jammu','Jamshedpur','Jodhpur','Kannur','Kochi','Kolhapur','Kota','Kurnool',
         'Lucknow','Malappuram','Goa','Meerut','Mysore','Nanded','Nellore','Palakkad','Perinthalmanna','Purulia',
         'Rajkot','Ranchi','Salem','Shimla','Solapur','Thiruvananthapuram','Tiruchirappalli','Tirupati','Tiruppur',
         'Ujjain','Vadodara','Vasai','Virar City','Vellore','Surat','Ajmer','Allahabad','Amritsar','Aurangabad',
         'Belgaum','Bhiwandi','Bhubaneswar','Bilaspur','Chandigarh','Cuttack','Dhanbad','Durgapur','Faridabad',
         'Ghaziabad','Gulbarga','Gwalior','Guwahati','Hamirpur','Jabalpur','Jalandhar','Jamnagar','Jhansi','Kakinada',
         'Kanpur','Kottayam','Kollam','Kozhikode','Ludhiana','Madurai','Mathura','Mangalore','Moradabad','Nagpur',
         'Nashik','Noida','Patna','Pondicherry','Raipur','Rajahmundry','Rourkela','Sangli','Siliguri','Srinagar',
         'Thrissur','Tirur','Tirunelveli','Tiruvannamalai','Bijapur','Varanasi','Vijayawada','Warangal','Visakhapatnam']



'''Function definitions'''

# function to create invoice-city pair dictionary
def make_invoice_city_dict(df,city_col="City"):
    # create a dict of invoice number and city
    inv_df = df.copy()
    inv_df = inv_df.set_index('InvoiceNo')
    invoice_city_dict = inv_df.to_dict()[city_col]    
    
    # return the dictionary
    return invoice_city_dict


# function to convert the city names to their respective tiers
def city_tiers(x):
    x = x.title()
    if x in Tier1: return "Tier1"
    elif x in Tier2: return "Tier2"
    else: return 'Tier3'


# function to choose the best model
def choose_model(init):

    # list all the models
    model_name_list = os.listdir(init['all_models']['path'])

    # create a list  of f1 scores
    f1score_list = list()
    for model in model_name_list:
        f1score = float(model.split('_f1score_')[1][:-4])
        f1score_list.append(f1score)

    # create a dataframe of model names and f1 scores
    scores_df = pd.DataFrame()
    scores_df['Model_name'] = model_name_list
    scores_df['F1_score'] = f1score_list


    # sort the scores_df with respect to f1 scores
    scores_df = scores_df.sort_values('F1_score',ascending=False).reset_index().iloc[:,1:]

    # choose the model with the best f1 score
    model_name = scores_df['Model_name'][0]

    # load the model to predict the output
    model_path = init['all_models']['path']
    model = pickle.load(open(model_path+model_name, 'rb'))

    # return the model
    return model



"""Main prediction function"""

# function to predict the output
def predict():
    
    # read the new data to predict
    df = pd.read_csv(init['divided_data']["new_df"]['path'])

    # create a dict of invoice number and city
    invoice_city_dict = make_invoice_city_dict(df,city_col='City')
    invoice_dlcity_dict = make_invoice_city_dict(df,city_col='DealerCity')
    
    
    # convert cities into tiers
    df['City'] = df['City'].apply(city_tiers)
    df['DealerCity'] = df['DealerCity'].apply(city_tiers)
    
    
    # label encode the required columns
    for col in label_encode_cols:
        label_encoder = LabelEncoder()
        encoder_dict[col] = label_encoder.fit(df[col])
        df[col] = encoder_dict[col].transform(df[col])
       
    
    # standardize the required columns
    for col in standardize_cols:
        mmscaler = MinMaxScaler()
        encoder_dict[col] = mmscaler.fit(df[col].values.reshape(-1, 1))
        df[col] = encoder_dict[col].transform(df[col].values.reshape(-1, 1))


    # load the model
    model = choose_model(init)


    # Set X and y for testing
    y = df['LoanTaken']
    X = df.drop('LoanTaken',axis=1)


    # calculate performance metrics for the model
    accuracy = SCORERS['accuracy'](model, X[classification_cols], y)
    precision = SCORERS['precision'](model, X[classification_cols], y)
    recall = SCORERS['recall'](model, X[classification_cols], y)
    f1_score = SCORERS['f1'](model, X[classification_cols], y)



    # predict the final outpput list
    ypredict = model.predict(X[classification_cols])
    ypredict_prob = model.predict_proba(X[classification_cols]).max(axis=1)
    
    
    # convert numpy arrays to pandas series
    ypredict = pd.Series(ypredict)
    ypredict_prob = pd.Series(ypredict_prob)
    

    # combine the final prediction with dataframe
    df_without_loan_col = df.reset_index().iloc[:,1:].drop('LoanTaken',axis=1)
    predicted_df = pd.concat([df_without_loan_col,ypredict,ypredict_prob],axis=1)
    predicted_df.columns = list(df_without_loan_col.columns) + ['LoanTaken_predict','Prediction_probability']
    
    
    # inverse label encode the columns
    for col in label_encode_cols:
        predicted_df[col] = encoder_dict[col].inverse_transform(predicted_df[col].astype(int))
        
    
    # inverse standardize the columns
    for col in standardize_cols:
        predicted_df[col] = encoder_dict[col].inverse_transform(predicted_df[col].values.reshape(-1,1))
        
        
    # convert cities to their original names from tiers
    predicted_df['City'] = predicted_df['InvoiceNo'].map(invoice_city_dict)
    predicted_df['DealerCity'] = predicted_df['InvoiceNo'].map(invoice_dlcity_dict)

        
    # log the performance scores
    prediction_log_path = init['prediction_log']['path']
    with open(prediction_log_path,'w+') as prediction_log_file:
        prediction_log_file.write("Accuracy: {}\n".format(accuracy))
        prediction_log_file.write("Precision: {}\n".format(precision))
        prediction_log_file.write("Recall: {}\n".format(recall))
        prediction_log_file.write("F1_Score: {}\n".format(f1_score))
    
    
    # save the predicted dataframe
    predicted_df.to_csv(init['prediction_data']["predicted_df"]['path'],index=False)