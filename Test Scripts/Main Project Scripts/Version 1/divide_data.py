# import required libraries
import pandas as pd
import numpy as np
import json

# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)

    
# divide the data into main_df and new_df
def divide(percentage=0.8):

    # read the merged data
    df = pd.read_csv(init["merged_data"]["merged_df"]["path"])

    # Divide the dataset into people taken loan and not taken loan
    loan_taken_df = df[df['LoanTaken']==1]
    loan_nottaken_df = df[df['LoanTaken']==0]

    # Divide the loan taken data into 80% and 20% data
    loan_taken_df1 = loan_taken_df.sample(frac=percentage)
    loan_taken_df2 = loan_taken_df[~loan_taken_df.isin(loan_taken_df1)].dropna()

    # Divide the loan not taken data into 80% and 20% data
    loan_nottaken_df1 = loan_nottaken_df.sample(frac=percentage)
    loan_nottaken_df2 = loan_nottaken_df[~loan_nottaken_df.isin(loan_nottaken_df1)].dropna()

    # Join the 80% datasets into main dataset and 20% dataset into new dataset
    main_df = pd.concat([loan_nottaken_df1,loan_taken_df1],axis=0)
    new_df = pd.concat([loan_nottaken_df2,loan_taken_df2],axis=0)

    # return the divided dataframes
    # return (main_df, new_df)

    # save the dataframes
    main_df.to_csv(init['divided_data']["main_df"]['path'],index=False)
    new_df.to_csv(init['divided_data']["new_df"]['path'],index=False)