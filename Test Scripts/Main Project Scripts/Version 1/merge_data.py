# import required dataframes
import pandas as pd
import numpy as np
import json

# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)

# function to make InvoiceNo column unique
def unique_invoice(df):
    # sort dataframe wrt InvoiceNo column 
    df = df.sort_values(['InvoiceNo'])

    # Generate cumulative count for each InvoiceNo
    cumulative_invoice_count = df.groupby(['InvoiceNo']).cumcount() + 1
    df['Invoice_count'] = cumulative_invoice_count

    # Add that cumulative count for InvoiceNo to dataframe
    df['Concat_invoice'] = df['InvoiceNo'] + "_" + df['Invoice_count'].astype(str)

    # Replace InvoiceNo with modifiied InvoiceNo
    df = df.drop(['InvoiceNo','Invoice_count'],axis=1)
    df = df.rename(columns={'Concat_invoice':'InvoiceNo'})

    # return the df with unique InvoiceNo
    return df

# function to merge retail and finance datasets
def merge_retail_finance():

    # read the cleaned datasets
    retail_df = pd.read_csv(init["cleaned_data"]["retail_df"]["path"])
    finance_df = pd.read_csv(init["cleaned_data"]["finance_df"]["path"])
    
    # merge the two datasets
    df = pd.merge(retail_df, finance_df, how = "left", on = "ChassisNo")

    # combine CustomerCode column and CustomerID column
    df['Customer_ID'] = np.where(df['CustomerID'].isnull(), df['CustomerCode'], df['CustomerID'].astype(str))
    df = df.drop(columns = ['CustomerCode', 'CustomerID'])
    df = df.rename(columns= {'Customer_ID': 'CustomerCode'})

    # combine ModelFamily columns
    df['ModelFamily'] = np.where(df['ModelFamily_x'].isnull(), df['ModelFamily_y'], df['ModelFamily_x'])
    df = df.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])

    # make InvoiceNo column unique
    df = unique_invoice(df)

    # convert LoanAmount column into a flag
    df['LoanTaken'] = np.where(df['LoanAmount'].isna(),0,1)
    df = df.drop('LoanAmount', axis=1)

    # drop duplicate rows
    df = df.groupby('ChassisNo').first().reset_index()
    df = df.drop_duplicates()
            
    # return the merged dataframe
    return df

# function to merge retail and enquiry datasets
def merge_retail_finance_enquiry():

    # read the cleaned datasets
    retail_finance_df = merge_retail_finance()
    enquiry_df = pd.read_csv(init["cleaned_data"]["enquiry_df"]["path"])

    # merge the two datasets
    df = pd.merge(retail_finance_df, enquiry_df, how = "left", on = ["Enquiry","State","ModelCode"])

    # combine BranchType columns
    df['BranchType'] = np.where(df['BranchType_y'].isnull(), df['BranchType_x'], df['BranchType_y'])
    df = df.drop(columns = ['BranchType_x', 'BranchType_y'])

    # combine City columns
    df['City'] = np.where(df['City_y'].isnull(), df['City_x'], df['City_y'])
    df = df.drop(columns = ['City_x', 'City_y'])

    # combine DealerCity columns
    df['DealerCity'] = np.where(df['DealerCity_y'].isnull(), df['DealerCity_x'], df['DealerCity_y'])
    df = df.drop(columns = ['DealerCity_x', 'DealerCity_y'])

    # combine TotalAmount columns
    df['TotalAmount'] = np.where(df['TotalAmount_y'].isnull(), df['TotalAmount_x'], df['TotalAmount_y'])
    df = df.drop(columns = ['TotalAmount_x', 'TotalAmount_y'])

    # combine ModelFamily columns
    df['ModelFamily'] = np.where(df['ModelFamily_y'].isnull(), df['ModelFamily_x'], df['ModelFamily_y'])
    df = df.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])

    # fill null values for LeadType column
    df['LeadType'] = df['LeadType'].fillna("No Enquiry")

    # fill null values for TestRideOffered column
    df['TestRideOffered'] = df['TestRideOffered'].fillna("No Enquiry")

    # fill null values for FollowUp column
    df['FollowUp'] = df['FollowUp'].fillna("No Enquiry")

    # fill null values for SourceOfEnquiry column
    df['SourceOfEnquiry'] = df['SourceOfEnquiry'].fillna("No Enquiry")

    # drop duplicate rows
    df = df.groupby(['Enquiry','State','ModelCode']).first().reset_index()
    df = df.drop_duplicates()

    # return the merged dataframe
    return df


# function to merge datasets
def merge():
    merged_df = merge_retail_finance_enquiry()
    # save the merged dataframe
    merged_df.to_csv(init['merged_data']["merged_df"]['path'],index=False)
