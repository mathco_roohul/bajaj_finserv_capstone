import pyodbc
import pandas as pd
import connection

# Create enquiry final table
def create_enq_fin_table():
    cursor, cnxn = connection.connect()
    create_enq_fin = """ 
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='enquiry_fin')
                CREATE TABLE finserv.enquiry_fin 
                (
                    ENQUIRY VARCHAR(25),
                    DEALER_CITY VARCHAR(30), 
                    BRANCH_TYPE VARCHAR(10), 
                    LEAD_TYPE VARCHAR(30), 
                    STATE VARCHAR(30), 
                    CITY VARCHAR(30), 
                    MODEL_FAMILY VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    TOTAL_AMOUNT FLOAT, 
                    TEST_RIDE_OFFERED VARCHAR(30), 
                    FOLLOW_UP VARCHAR(30), 
                    SRC_OF_ENQ VARCHAR(50),
                    UPDATE_DATE DATE
                )
    """
    cursor.execute(create_enq_fin)
    cursor.commit()
    connection.close_conn(cnxn)

# Create retail final table
def create_ret_fin_table():
    cursor, cnxn = connection.connect()
    create_ret_fin = """
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='retail_fin')
                CREATE TABLE finserv.retail_fin 
                (
                    ENQUIRY VARCHAR(25), 
                    STATE VARCHAR(30), 
                    INVOICE_NO VARCHAR(30), 
                    INVOICE_DATE VARCHAR(12),  
                    DEALER_CITY VARCHAR(30), 
                    BRANCH_TYPE VARCHAR(10), 
                    CUSTOMER_CODE VARCHAR(30), 
                    CITY VARCHAR(30), 
                    MODEL_FAMILY VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    COLOR VARCHAR(50), 
                    CHASSIS_NO VARCHAR(30), 
                    TOTAL_AMOUNT FLOAT, 
                    SEGMENT VARCHAR(5), 
                    ENQUIRY_FLAG INT,
                    BOOKING_FLAG INT,
                    UPDATE_DATE DATE
                )
    """
    cursor.execute(create_ret_fin)
    cursor.commit()
    connection.close_conn(cnxn)

# Create merged final table
def create_merged_fin_table():
    cursor, cnxn = connection.connect()
    create_merged_fin = """
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='merged_fin')
                CREATE TABLE finserv.merged_fin 
                (
                    ENQUIRY VARCHAR(25), 
                    STATE VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    CHASSIS_NO VARCHAR(30), 
                    INVOICE_DATE VARCHAR(12), 
                    COLOR VARCHAR(50), 
                    SEGMENT VARCHAR(5), 
                    ENQUIRY_FLAG INT, 
                    BOOKING_FLAG INT, 
                    CUSTOMER_CODE VARCHAR(30), 
                    INVOICE_NO VARCHAR(30), 
                    LEAD_TYPE VARCHAR(30),
                    TEST_RIDE_OFFERED VARCHAR(30), 
                    FOLLOW_UP VARCHAR(30),
                    SRC_OF_ENQ VARCHAR(50), 
                    BRANCH_TYPE VARCHAR(10), 
                    CITY VARCHAR(30), 
                    DEALER_CITY VARCHAR(30), 
                    TOTAL_AMOUNT FLOAT, 
                    MODEL_FAMILY VARCHAR(30),
                    UPDATE_DATE DATE
                )
    """
    cursor.execute(create_merged_fin)
    cursor.commit()
    connection.close_conn(cnxn)

# Create prediction final table
def create_pred_fin_table():
    cursor, cnxn = connection.connect()
    create_pred_fin = """
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='predicted_fin')
                CREATE TABLE finserv.predicted_fin 
                (
                    ENQUIRY VARCHAR(25), 
                    STATE VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    CHASSIS_NO VARCHAR(30), 
                    INVOICE_DATE VARCHAR(12), 
                    COLOR VARCHAR(50), 
                    SEGMENT VARCHAR(5), 
                    ENQUIRY_FLAG INT, 
                    BOOKING_FLAG INT, 
                    CUSTOMER_CODE VARCHAR(30), 
                    INVOICE_NO VARCHAR(30), 
                    LEAD_TYPE VARCHAR(30),
                    TEST_RIDE_OFFERED VARCHAR(30), 
                    FOLLOW_UP VARCHAR(30),
                    SRC_OF_ENQ VARCHAR(50), 
                    BRANCH_TYPE VARCHAR(10), 
                    CITY VARCHAR(30), 
                    DEALER_CITY VARCHAR(30), 
                    TOTAL_AMOUNT FLOAT, 
                    MODEL_FAMILY VARCHAR(30),
                    LOAN_TAKEN_PREDICT INT,
                    LOAN_TAKEN_PROBABILITY FLOAT,
                    LOAN_TAKEN_TAG INT,
                    UPLOAD_DATE DATE
                )
    """
    cursor.execute(create_pred_fin)
    cursor.commit()
    connection.close_conn(cnxn)


# Create enquiry staging table
def create_enq_stg_table():
    cursor, cnxn = connection.connect()
    create_enq_stg = """ 
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='enquiry_stg')
                CREATE TABLE finserv.enquiry_stg 
                (
                    ENQUIRY VARCHAR(25),
                    DEALER_CITY VARCHAR(30), 
                    BRANCH_TYPE VARCHAR(10), 
                    LEAD_TYPE VARCHAR(30), 
                    STATE VARCHAR(30), 
                    CITY VARCHAR(30), 
                    MODEL_FAMILY VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    TOTAL_AMOUNT FLOAT, 
                    TEST_RIDE_OFFERED VARCHAR(30), 
                    FOLLOW_UP VARCHAR(30), 
                    SRC_OF_ENQ VARCHAR(50)
                )
    """
    cursor.execute(create_enq_stg)
    cursor.commit()
    connection.close_conn(cnxn)

# Create retail staging table
def create_ret_stg_table():
    cursor, cnxn = connection.connect()
    create_ret_stg = """
            IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
            WHERE table_name='retail_stg')
            CREATE TABLE finserv.retail_stg
            (
                ENQUIRY VARCHAR(25), 
                STATE VARCHAR(30), 
                INVOICE_NO VARCHAR(30), 
                INVOICE_DATE VARCHAR(12),  
                DEALER_CITY VARCHAR(30), 
                BRANCH_TYPE VARCHAR(10), 
                CUSTOMER_CODE VARCHAR(30), 
                CITY VARCHAR(30), 
                MODEL_FAMILY VARCHAR(30), 
                MODEL_CODE VARCHAR(10), 
                COLOR VARCHAR(50), 
                CHASSIS_NO VARCHAR(30), 
                TOTAL_AMOUNT FLOAT, 
                SEGMENT VARCHAR(5), 
                ENQUIRY_FLAG INT,
                BOOKING_FLAG INT
            )
    """
    cursor.execute(create_ret_stg)
    cursor.commit()
    connection.close_conn(cnxn)

# Create merged staging table
def create_merged_stg_table():
    cursor, cnxn = connection.connect()
    create_merged_stg = """
                IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='merged_stg')
                CREATE TABLE finserv.merged_stg 
                (
                    ENQUIRY VARCHAR(25), 
                    STATE VARCHAR(30), 
                    MODEL_CODE VARCHAR(10), 
                    CHASSIS_NO VARCHAR(30), 
                    INVOICE_DATE VARCHAR(12), 
                    COLOR VARCHAR(50), 
                    SEGMENT VARCHAR(5), 
                    ENQUIRY_FLAG INT, 
                    BOOKING_FLAG INT, 
                    CUSTOMER_CODE VARCHAR(30), 
                    INVOICE_NO VARCHAR(30), 
                    LEAD_TYPE VARCHAR(30),
                    TEST_RIDE_OFFERED VARCHAR(30), 
                    FOLLOW_UP VARCHAR(30),
                    SRC_OF_ENQ VARCHAR(50), 
                    BRANCH_TYPE VARCHAR(10), 
                    CITY VARCHAR(30), 
                    DEALER_CITY VARCHAR(30), 
                    TOTAL_AMOUNT FLOAT, 
                    MODEL_FAMILY VARCHAR(30)
                )
    """
    cursor.execute(create_merged_stg)
    cursor.commit()
    connection.close_conn(cnxn)

# Drop enquiry staging table
def drop_enq_stg_table():
    cursor, cnxn = connection.connect()
    drop_enq_stg = """
                IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='enquiry_stg')
                DROP table finserv.enquiry_stg
    """
    cursor.execute(drop_enq_stg)
    cursor.commit()
    connection.close_conn(cnxn)

# Drop retail staging table
def drop_ret_stg_table():
    cursor, cnxn = connection.connect()
    drop_ret_stg = """
                IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='retail_stg')
                DROP table finserv.retail_stg
    """
    cursor.execute(drop_ret_stg)
    cursor.commit()
    connection.close_conn(cnxn)

# Drop enquiry staging table
def drop_merged_stg_table():
    cursor, cnxn = connection.connect()
    drop_merged_stg = """
                IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.tables
                WHERE table_name='merged_stg')
                DROP table finserv.merged_stg
    """
    cursor.execute(drop_merged_stg)
    cursor.commit()
    connection.close_conn(cnxn)