# import required libraries
import azure.storage.blob
import os, uuid, sys
from azure.storage.blob import BlockBlobService, PublicAccess, BlobPermissions
from azure.storage.blob.baseblobservice import BaseBlobService
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import sys
import os


class Blob:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            # select error log file path
            self.error_log_path = "Test Scripts\Main Project Scripts\Version 5\Tasks\error_log.txt"
            # create blob credentials
            self.account_name = "capstoneblob2"
            self.account_key = "m1tER8t0AIbFJaHO/bnRRp4omvKaU5+qgSO72xgRIccJBC+CIzfamItedpWBRt7MZd7TLFjOfTzdU0+joQ771A=="
            self.container_name ='finserv'
            # create blob services
            self.block_blob_service = BlockBlobService(account_name=self.account_name, account_key=self.account_key)
            self.base_blob_service = BaseBlobService(account_name=self.account_name, account_key=self.account_key)
            # check if connection is succcessful
            self.generator = self.block_blob_service.list_blobs(self.container_name)
            for blob in self.generator:
                print("\t Blob name: " + blob.name)
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed init argument is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)

# ----------------------------------------------------------------------------------------------------------------------

    # Upload data to blob storage
    def upload_data(self,filename,output_file):
        # upload data to blob storage
        try:
            self.block_blob_service.create_blob_from_text(self.container_name, filename, output_file)
            # return True if the process is completed
            return True
        except Exception as e:
            a = "\tLocation: Uploading data to blob.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="upload_data")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # Download data from blob
    def download_data(self,filename):
        try:
            # set initial tokens to read data
            self.sas_token = self.base_blob_service.generate_blob_shared_access_signature(self.container_name, 
                        filename, permission=BlobPermissions.READ, expiry=datetime.utcnow() + timedelta(hours=1))
            self.blob_url_with_sas = self.base_blob_service.make_blob_url(self.container_name, filename, sas_token=self.sas_token)
            # read data
            self.input_df = pd.read_csv(self.blob_url_with_sas)
            # return data if the process is completed
            return self.input_df
        except Exception as e:
            a = "\tLocation: Downloading data from blob.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            print(e)
            self.log_errors(error=e,function_name="download_data")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # delete a blob (file) in blob storage
    def delete_data(self,filename):
        # delete the file
        try:
            self.block_blob_service.delete_blob(self.container_name, filename)
            # return True if the process is completed
            return True
        except Exception as e:
            a = "\tLocation: Deleting data from blob.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="delete_data")
            return False
    
# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Blob", script_name="BlobConnect.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))