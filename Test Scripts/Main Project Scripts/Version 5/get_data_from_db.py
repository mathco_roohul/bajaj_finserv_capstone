import pandas as pd
import connection

#extract data from enquiry staging table
def get_enq_stg_data():
    cursor, cnxn = connection.connect()
    get_enq_stg = """
            SELECT * FROM finserv.enquiry_stg
    """
    enq_stg_df = pd.read_sql(get_enq_stg, cnxn)
    enq_stg_df.columns = ['Enquiry', 'DealerCity', 'BranchType', 'LeadType', 'State', 'City', 'ModelFamily', 'ModelCode', 'TotalAmount', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry']
    connection.close_conn(cnxn)
    return enq_stg_df

#extract data from retail staging table
def get_ret_stg_data():
    cursor, cnxn = connection.connect()
    get_ret_stg = """
            SELECT * FROM finserv.retail_stg
    """
    ret_stg_df = pd.read_sql(get_ret_stg, cnxn)
    ret_stg_df.columns = ['Enquiry', 'State', 'InvoiceNo', 'InvoiceDate', 'DealerCity', 'BranchType', 'CustomerCode', 'City', 'ModelFamily', 'ModelCode', 'Color', 'ChassisNo', 'TotalAmount', 'Segment', 'Enquiry_flag', 'Booking_flag']
    connection.close_conn(cnxn)
    return ret_stg_df

#extract data from merged staging table
def get_merged_stg_data():
    cursor, cnxn = connection.connect()
    get_merged_stg = """
            SELECT * FROM finserv.merged_stg
    """
    merged_stg_df = pd.read_sql(get_merged_stg, cnxn)
    merged_stg_df.columns = ['Enquiry', 'State', 'ModelCode', 'ChassisNo', 'InvoiceDate', 'Color', 'Segment', 'Enquiry_flag', 'Booking_flag', 'CustomerCode', 'InvoiceNo', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'DealerCity', 'TotalAmount', 'ModelFamily']
    connection.close_conn(cnxn)
    return merged_stg_df

#extract data from enquiry fin table
def get_enq_fin_data():
    cursor, cnxn = connection.connect()
    get_enq_fin = """
            SELECT * FROM finserv.enquiry_fin
    """
    enq_stg_df = pd.read_sql(get_enq_fin, cnxn)
    enq_stg_df.columns = ['Enquiry', 'DealerCity', 'BranchType', 'LeadType', 'State', 'City', 'ModelFamily', 'ModelCode', 'TotalAmount', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'UploadDate']
    connection.close_conn(cnxn)
    return enq_stg_df

#extract data from retail final table
def get_ret_fin_data():
    cursor, cnxn = connection.connect()
    get_ret_stg = """
            SELECT * FROM finserv.retail_fin
    """
    ret_fin_df = pd.read_sql(get_ret_stg, cnxn)
    ret_fin_df.columns = ['Enquiry', 'State', 'InvoiceNo', 'InvoiceDate', 'DealerCity', 'BranchType', 'CustomerCode', 'City', 'ModelFamily', 'ModelCode', 'Color', 'ChassisNo', 'TotalAmount', 'Segment', 'Enquiry_flag', 'Booking_flag', 'UploadDate']
    connection.close_conn(cnxn)
    return ret_fin_df

#extract data from merged final table
def get_merged_fin_data():
    cursor, cnxn = connection.connect()
    get_merged_fin = """
            SELECT * FROM finserv.merged_fin
    """
    merged_fin_df = pd.read_sql(get_merged_fin, cnxn)
    merged_fin_df.columns = ['Enquiry', 'State', 'ModelCode', 'ChassisNo', 'InvoiceDate', 'Color', 'Segment', 'Enquiry_flag', 'Booking_flag', 'CustomerCode', 'InvoiceNo', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'DealerCity', 'TotalAmount', 'ModelFamily', 'UploadDate']
    connection.close_conn(cnxn)
    return merged_fin_df









