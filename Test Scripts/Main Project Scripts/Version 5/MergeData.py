# import required dataframes
import json
import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime
import connection_to_db
import insert_data_to_db
import get_data_from_db
import create_and_drop_db

# class to merge data
class Merge:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            with open('init.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed filename or filetype is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to make InvoiceNo column unique
    def unique_invoice(self,df):

        try:
            # make a copy of df to work
            self.uniq_in_df = df.copy()

            # sort dataframe wrt InvoiceNo column 
            self.uniq_in_df = self.uniq_in_df.sort_values(['InvoiceNo'])

            # Generate cumulative count for each InvoiceNo
            self.uniq_in_df['Invoice_count'] = self.uniq_in_df.groupby(['InvoiceNo']).cumcount() + 1

            # Add that cumulative count for InvoiceNo to dataframe
            self.uniq_in_df['Concat_invoice'] = self.uniq_in_df['InvoiceNo'] + "_" + self.uniq_in_df['Invoice_count'].astype(str)

            # Replace InvoiceNo with modifiied InvoiceNo
            self.uniq_in_df = self.uniq_in_df.drop(['InvoiceNo','Invoice_count'],axis=1)
            self.uniq_in_df = self.uniq_in_df.rename(columns={'Concat_invoice':'InvoiceNo'})

            # return the df with unique InvoiceNo
            return self.uniq_in_df

        except Exception as e:
            a = "\tLocation: Unique Invoice Function.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="unique_invoice")
            return False

# ----------------------------------------------------------------------------------------------------------------------


    # function to merge retail and enquiry datasets
    def merge_retail_enquiry(self, retail_df, enquiry_df):
        # merge the two datasets
        try:
            self.merged_df = pd.merge(self.retail_df, self.enquiry_df, how = "left", on = ["Enquiry","State","ModelCode"])
        except Exception as e:
            a = "\tLocation: Merging data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine BranchType columns
        try:
            self.merged_df['BranchType'] = np.where(self.merged_df['BranchType_y'].isnull(), self.merged_df['BranchType_x'], self.merged_df['BranchType_y'])
            self.merged_df = self.merged_df.drop(columns = ['BranchType_x', 'BranchType_y'])
        except Exception as e:
            a = "\tLocation: Combining branch type columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine City columns
        try:
            self.merged_df['City'] = np.where(self.merged_df['City_y'].isnull(), self.merged_df['City_x'], self.merged_df['City_y'])
            self.merged_df = self.merged_df.drop(columns = ['City_x', 'City_y'])
        except Exception as e:
            a = "\tLocation: Combining city columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine DealerCity columns
        try:
            self.merged_df['DealerCity'] = np.where(self.merged_df['DealerCity_y'].isnull(), self.merged_df['DealerCity_x'], self.merged_df['DealerCity_y'])
            self.merged_df = self.merged_df.drop(columns = ['DealerCity_x', 'DealerCity_y'])
        except Exception as e:
            a = "\tLocation: Combining DealerCity columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine TotalAmount columns
        try:
            self.merged_df['TotalAmount'] = np.where(self.merged_df['TotalAmount_y'].isnull(), self.merged_df['TotalAmount_x'], self.merged_df['TotalAmount_y'])
            self.merged_df = self.merged_df.drop(columns = ['TotalAmount_x', 'TotalAmount_y'])
        except Exception as e:
            a = "\tLocation: Combining TotalAmount columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine ModelFamily columns
        try:
            self.merged_df['ModelFamily'] = np.where(self.merged_df['ModelFamily_y'].isnull(), self.merged_df['ModelFamily_x'], self.merged_df['ModelFamily_y'])
            self.merged_df = self.merged_df.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])
        except Exception as e:
            a = "\tLocation: Combining model family columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # fill null values
        try:
            self.merged_df['LeadType'] = self.merged_df['LeadType'].fillna("No Enquiry")
            self.merged_df['TestRideOffered'] = self.merged_df['TestRideOffered'].fillna("No Enquiry")
            self.merged_df['FollowUp'] = self.merged_df['FollowUp'].fillna("No Enquiry")
            self.merged_df['SourceOfEnquiry'] = self.merged_df['SourceOfEnquiry'].fillna("No Enquiry")
        except Exception as e:
            a = "\tLocation: Filling null values.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # drop duplicate rows
        try:
            self.merged_df = self.merged_df.groupby(['Enquiry','State','ModelCode']).first().reset_index()
            self.merged_df = self.merged_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # drop null rows
        try:
            self.merged_df = self.merged_df.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Dropping null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        return merged_df

# ----------------------------------------------------------------------------------------------------------------------


    # function to merge merged dataset and finance dataset
    def merge_finance(self):

        # read the finance data
        # check if a file exists, and read if it does
        try:
            self.finance_data_dir = self.init['cleaned_data']['finance_df']['path']
            self.finance_filename = sorted(os.listdir(self.finance_data_dir))[-1]
            self.finance_file_date = pd.to_datetime(self.finance_filename.split('_')[0])
            self.finance_df = pd.read_csv(self.finance_data_dir+self.finance_filename)
        except IndexError:
            e = "There is no file in the finance data directory, please check the folder and run the code again"
            a = "\tLocation: Reading Finance Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False
        except KeyError:
            e = "The folder/file name for finance_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Reading Finance Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False
        except ValueError:
            e = "The date in the filename is not properly formatted, fix it and try again. Format = yyyymmdd_filename.csv"
            a = "\tLocation: Reading Finance Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
        except Exception as e:
            a = "\tLocation: Reading Finance Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False
        
        # read merged data and date
        try:
            self.merged_data, self.merged_file_date = self.merge_retail_enquiry(save=False)
        except Exception as e:
            a = "\tLocation: Reading Merged Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # check if both merged and finance dataset's dates are same
        if self.merged_file_date != self.finance_file_date:
            e = "The dates of merged and finance datasets do not match, check the folder and rename it accordingly"
            a = "\tLocation: Checking dates of merged and finance datasets.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False
        
        # merge the two datasets
        try:
            self.merged_df_finance = pd.merge(self.merged_df, self.finance_df, how = "left", on = "ChassisNo")
        except Exception as e:
            a = "\tLocation: Merging data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # combine CustomerCode column and CustomerID column
        try:
            self.merged_df_finance['Customer_ID'] = np.where(self.merged_df_finance['CustomerID'].isnull(), self.merged_df_finance['CustomerCode'], self.merged_df_finance['CustomerID'])
            self.merged_df_finance = self.merged_df_finance.drop(columns = ['CustomerCode', 'CustomerID'])
            self.merged_df_finance = self.merged_df_finance.rename(columns= {'Customer_ID': 'CustomerCode'})
        except Exception as e:
            a = "\tLocation: Combining customer code and id columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # combine ModelFamily columns
        try:
            self.merged_df_finance['ModelFamily'] = np.where(self.merged_df_finance['ModelFamily_x'].isnull(), self.merged_df_finance['ModelFamily_y'], self.merged_df_finance['ModelFamily_x'])
            self.merged_df_finance = self.merged_df_finance.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])
        except Exception as e:
            a = "\tLocation: Combining model family columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # convert LoanAmount column into a flag
        try:
            self.merged_df_finance['LoanTaken'] = np.where(self.merged_df_finance['LoanAmount'].isna(),0,1)
            self.merged_df_finance = self.merged_df_finance.drop('LoanAmount', axis=1)
        except Exception as e:
            a = "\tLocation: Converting loan amount column into flag.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # drop duplicate rows
        try:
            self.merged_df_finance = self.merged_df_finance.groupby(['Enquiry','State','ModelCode']).first().reset_index()
            self.merged_df_finance = self.merged_df_finance.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # drop null rows
        try:
            self.merged_df_finance = self.merged_df_finance.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Dropping null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # save the dataframe
        try:
            # choosing any dataset's date
            self.merged_df_finance_file_date = self.finance_file_date
            self.merged_df_finance_file_date_str = self.merged_df_finance_file_date.strftime("%Y%m%d")
            self.merged_df_finance_filename = self.merged_df_finance_file_date_str+"_Merged_data_with_finance.csv"
            self.merged_df_finance.to_csv(self.init['merged_data']['merged_df_finance']['path']+self.merged_df_finance_filename,index=False)
        except KeyError:
            e = "The folder/file name for merged_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Saving data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False
        except Exception as e:
            a = "\tLocation: Saving data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # if all worked well, return True
        return True


# ----------------------------------------------------------------------------------------------------------------------

    # logging errors function
    def log_errors(self, error, function_name, class_name="Merge", script_name="MergeData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
