import datetime
import logging
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import  BashOperator
from airflow.utils.dates import days_ago
import subprocess
import signal
import os
import json

def greet():
   print("Start Data Pipeline")

args = {
    "owner": "bajaj",
    "retries": 1,
    "start_date": days_ago(2),
}

dag = DAG('finserv_train_pipeline_v1', default_args = args, tags = ['Bajaj Finserv'], start_date = datetime.datetime.now())
venv_path = "/home/azureuser/bajaj/bajaj_env/bin/activate"
check_data_path = "/home/azureuser/bajaj/capstone/t_check_data.py"
clean_enquiry_path = "/home/azureuser/bajaj/capstone/t_clean_enquiry_v2.py"
clean_retail_path = "/home/azureuser/bajaj/capstone/t_clean_retail_v2.py"
merge_data_path = '/home/azureuser/bajaj/capstone/t_merge_data_v2.py'
upload_cleaned_retail_path = '/home/azureuser/bajaj/capstone/t_upload_clean_retail_data.py'
upload_cleaned_enquiry_path = '/home/azureuser/bajaj/capstone/t_upload_clean_enquiry_data.py'
upload_merged_data_path = '/home/azureuser/bajaj/capstone/t_upload_merge_data.py'
predict_path = '/home/azureuser/bajaj/capstone/t_predict.py_v2'
upload_predict_path = '/home/azureuser/bajaj/capstone/t_upload_predict_data.py'



def python_venv_command(path):

    command = ". " + venv_path + " &&  python " + path
    #command = command.replace('\n', '')
    return command

def run_shell_command(command):
    last_stdout_line = ""
    proc = None
    print(command)
    try:
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (output, errors) = proc.communicate()
        if proc.returncode:
            raise RuntimeError('error running command: %s. Return Code: %d,%s' % (command,proc.returncode,errors))
        try:
            last_stdout_line = output.decode("utf-8").split("\n")[-2]
            print("last stdoutline: " + json.dumps(last_stdout_line))
            print("script ran well")
        except Exception as ex1:
            print("error" + str(ex1))  
    except Exception as ex2:
        print("an error occured" + str(ex2))
        raise
    finally:
        try:
            if proc and hasattr(proc, 'pid'):
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        except Exception as ex3:
            print("No process found" + str(ex3))
    return last_stdout_line

def check_data(**kwargs):
    command = python_venv_command(check_data_path)
    result_string = run_shell_command(command)
    return result_string

def clean_enquiry(**kwargs):
    command = python_venv_command(clean_enquiry_path)
    result_string = run_shell_command(command)
    return result_string

def clean_retail(**kwargs):
    command = python_venv_command(clean_retail_path)
    result_string = run_shell_command(command)
    return result_string

def merge_data(**kwargs):
    command = python_venv_command(merge_data_path)
    result_string = run_shell_command(command)
    return result_string

def predict(**kwargs):
    command = python_venv_command(predict_path)
    result_string = run_shell_command(command)
    return result_string

def upload_clean_retail(**kwargs):
    command = python_venv_command(upload_cleaned_retail_path)
    result_string = run_shell_command(command)
    return result_string

def upload_clean_enquiry(**kwargs):
    command = python_venv_command(upload_cleaned_enquiry_path)
    result_string = run_shell_command(command)
    return result_string

def upload_merged_data(**kwargs):
    command = python_venv_command(upload_merged_data_path)
    result_string = run_shell_command(command)
    return result_string

def upload_predict_data(**kwargs):
    command = python_venv_command(upload_predict_path)
    result_string = run_shell_command(command)
    return result_string


check_data_columns = DummyOperator(task_id='check_data_columns',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

clean_finance_data = DummyOperator(task_id='clean_finance_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

merge_with_retail_and_enquiry =  DummyOperator(task_id='merge_with_retail_and_enquiry',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

divide_data =  DummyOperator(task_id='divide_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

training_pipeline_1 = DummyOperator(task_id='training_pipeline_1',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

training_pipeline_2 = DummyOperator(task_id='training_pipeline_2',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

training_pipeline_3 = DummyOperator(task_id='training_pipeline_3',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

training_pipeline_4 = DummyOperator(task_id='training_pipeline_4',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

training_pipeline_5 = DummyOperator(task_id='training_pipeline_5',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

predict_results =  DummyOperator(task_id='predict_results',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

upload_clean_finance_data =  DummyOperator(task_id='upload_clean_finance_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

upload_merge_data =  DummyOperator(task_id='upload_merge_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

upload_main_data =  DummyOperator(task_id='upload_main_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

upload_new_data =  DummyOperator(task_id='upload_new_data',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)

save_best_model =  DummyOperator(task_id='save_best_model',
                        trigger_rule ='none_failed_or_skipped',
                        dag = dag)



check_data_columns >> clean_finance_data >> upload_clean_finance_data
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> upload_main_data
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> training_pipeline_3 >> predict_results >> save_best_model
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> training_pipeline_5 >> predict_results >> save_best_model
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> training_pipeline_2 >> predict_results >> save_best_model
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> training_pipeline_4 >> predict_results >> save_best_model
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> training_pipeline_1 >> predict_results >> save_best_model
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> divide_data >> upload_new_data
check_data_columns >> clean_finance_data >> merge_with_retail_and_enquiry >> upload_merge_data



