# import required dataframes
import json
import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime

# import the train test split to divide the data into two parts
from sklearn.model_selection import train_test_split


# class to divide data
class Divide:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed filename or filetype is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to divide the data
    def divide_data(self,percentage=0.8):

        # read the merged data
        # check if a file exists, and read if it does
        try:
            self.merged_data_dir = self.init['merged_data']['merged_df_finance']['path']
            self.merged_filename = sorted(os.listdir(self.merged_data_dir))[-1]
            self.merged_file_date = pd.to_datetime(self.merged_filename.split('_')[0])
            self.merged_df = pd.read_csv(self.merged_data_dir+self.merged_filename)
        except IndexError:
            e = "There is no file in the merged data directory, please check the folder and run the code again"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        except KeyError:
            e = "The folder/file name for merged_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        except ValueError:
            e = "The date in the filename is not properly formatted, fix it and try again. Format = yyyymmdd_filename.csv"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
        except Exception as e:
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False

        # sort the dataset with respect to time
        try:
            self.merged_df = self.merged_df.sort_values('InvoiceDate')
        except Exception as e:
            a = "\tLocation: Sort the dataset with respect to time.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False

        # divide data into loan taken and not taken
        try:
            # Divide the dataset into people taken loan and not taken loan
            self.loan_taken_df = self.merged_df[self.merged_df['LoanTaken']==1]
            self.loan_nottaken_df = self.merged_df[self.merged_df['LoanTaken']==0]
            # divide the data into two parts 
            self.loan_taken_df1, self.loan_taken_df2 = train_test_split(self.loan_taken_df, train_size = percentage, shuffle=False)
            self.loan_nottaken_df1, self.loan_nottaken_df2 = train_test_split(self.loan_nottaken_df, train_size = percentage, shuffle=False)
        except Exception as e:
            a = "\tLocation: Divide data into loan taken and not taken.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        
        # Join the 80% datasets into main dataset and 20% dataset into new dataset
        try:
            self.main_df = pd.concat([self.loan_nottaken_df1,self.loan_taken_df1],axis=0)
            self.new_df = pd.concat([self.loan_nottaken_df2,self.loan_taken_df2],axis=0)
        except Exception as e:
            a = "\tLocation: Joining datasets.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        
        # save the main_df dataframe
        try:
            self.main_file_date_str = self.merged_file_date.strftime("%Y%m%d")
            self.main_data_filename = self.main_file_date_str+"_Main_data.csv"
            self.main_df.to_csv(self.init['divided_data']['main_df']['path']+self.main_data_filename,index=False)
        except KeyError:
            e = "The folder/file name for main_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Saving main_df data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        except Exception as e:
            a = "\tLocation: Saving main_df data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False

        # save the new_df dataframe
        try:
            self.new_file_date_str = self.merged_file_date.strftime("%Y%m%d")
            self.new_data_filename = self.new_file_date_str+"_New_data.csv"
            self.new_df.to_csv(self.init['divided_data']['new_df']['path']+self.new_data_filename,index=False)
        except KeyError:
            e = "The folder/file name for new_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Saving new_df data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        except Exception as e:
            a = "\tLocation: Saving new_df data as csv.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="divide_data")
            return False
        
        # if all worked well, return True
        return True

# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Divide", script_name="DivideData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))