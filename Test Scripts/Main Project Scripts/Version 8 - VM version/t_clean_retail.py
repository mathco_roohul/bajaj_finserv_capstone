import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from CleanData import Clean
from BlobConnect import Blob
import json
import io

with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']
#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="Clean", script_name="clean_retail.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))


#--Extract retail.csv from blob---------------------------------------------

try:
    output = io.StringIO()
    blob = Blob()
    retail_df = blob.download_data("Sample_data/Retail_data/20201012_Retail_data_sample.csv")
except Exception as e:
    a = "\tLocation: Reading retail data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#save file in retail_df dataframe
#---------------------------------------------------------------------------

#--Performing Cleaning of Retail Data---------------------------------------

clean_obj = Clean()
cleaned_retail_df = clean_obj.clean_retail_data(retail_df)

#---------------------------------------------------------------------------



#--Pushing data into SQL database-------------------------------------------

#Pushing data into SQL staging table
try:
    insert_data_to_db.insert_ret_stg_table(cleaned_retail_df)
    print("Inserted cleaned retail data into staging table successfully.")
except Exception as e:
    a = "\tLocation: Retail Staging Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_ret_stg_table")

#Pushing data into SQL final table
try:
    insert_data_to_db.insert_ret_fin_table(cleaned_retail_df)
    print("Inserted cleaned retail data into final table successfully.")
except Exception as e:
    a = "\tLocation: Retail Final Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_ret_fin_table")

#--Pushing data into blob---------------------------------------------------

try:
    output = cleaned_retail_df.to_csv(index_label="idx", encoding = "utf-8", index=False)
    blob.upload_data("Cleaned_data/Retail_data/20201012_Retail_data_cleaned.csv",output)
except Exception as e:
    a = "\tLocation: Pushing cleaned retail data into the blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="push_data_to_blob")
#---------------------------------------------------------------------------