import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from BlobConnect import Blob
import json
import io

with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']
#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="Predict", script_name="t_upload_predict_data.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))

#--Extract predicted data from blob---------------------------------------------

try:
    output = io.StringIO()
    blob = Blob()
    predicted_df = blob.download_data("Prediction_data/Full_data/20201012_Full_Predicted_data.csv")
except Exception as e:
    a = "\tLocation: Reading predicted data from blob.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="Read_data_from_blob")

#save the data into the predicted_fin table
try:
    insert_data_to_db.insert_pred_fin_table(predicted_df)
    print("Inserted predicted data into final table successfully.")
except Exception as e:
    a = "\tLocation: Predicted Final Table Data Insert.\n"
    b = "\tProblem: "
    e = a+b+str(e)
    log_errors(error=e,function_name="insert_pred_fin_table")