# imort required libraries
import pandas as pd
import numpy as np
import json
import pickle
import os

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler

# import model evaluation library
from sklearn.metrics import SCORERS


# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)


'''Initializations - Variables'''

# create initial variables
count = 0
invoice_city_dict = dict()
invoice_dlcity_dict = dict()
encoder_dict = dict()
prediction_performance_list = list()


'''Initialization - Lists'''

classification_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'TotalAmount', 'City', 'State', 'DealerCity', 'ModelFamily']
no_classification_cols = ['Enquiry','ModelCode','ChassisNo','InvoiceDate','Color','CustomerCode','InvoiceNo']
label_encode_cols = ['Segment', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
standardize_cols = ['TotalAmount']
to_ohe = ['Segment','LeadType','TestRideOffered','FollowUp','SourceOfEnquiry','BranchType','City','State','DealerCity','ModelFamily']
not_to_ohe = ['Enquiry_flag','Booking_flag','TotalAmount','LoanTaken']
# List down Tier 1 and 2 companies
Tier1 = ['Bengaluru','Chennai','Delhi','Hyderabad','Kolkata','Mumbai','Ahmedabad','Pune']
Tier2 = ['Agra','Aligarh','Amravati','Asansol','Bareilly','Bhavnagar','Bhopal','Bikaner','Bokaro Steel City',
         'Coimbatore','Dehradun','Bhilai','Erode','Firozabad','Gorakhpur','Guntur','Gurgaon','Hubli','Dharwad',
         'Indore','Jaipur','Jammu','Jamshedpur','Jodhpur','Kannur','Kochi','Kolhapur','Kota','Kurnool',
         'Lucknow','Malappuram','Goa','Meerut','Mysore','Nanded','Nellore','Palakkad','Perinthalmanna','Purulia',
         'Rajkot','Ranchi','Salem','Shimla','Solapur','Thiruvananthapuram','Tiruchirappalli','Tirupati','Tiruppur',
         'Ujjain','Vadodara','Vasai','Virar City','Vellore','Surat','Ajmer','Allahabad','Amritsar','Aurangabad',
         'Belgaum','Bhiwandi','Bhubaneswar','Bilaspur','Chandigarh','Cuttack','Dhanbad','Durgapur','Faridabad',
         'Ghaziabad','Gulbarga','Gwalior','Guwahati','Hamirpur','Jabalpur','Jalandhar','Jamnagar','Jhansi','Kakinada',
         'Kanpur','Kottayam','Kollam','Kozhikode','Ludhiana','Madurai','Mathura','Mangalore','Moradabad','Nagpur',
         'Nashik','Noida','Patna','Pondicherry','Raipur','Rajahmundry','Rourkela','Sangli','Siliguri','Srinagar',
         'Thrissur','Tirur','Tirunelveli','Tiruvannamalai','Bijapur','Varanasi','Vijayawada','Warangal','Visakhapatnam']



'''Function definitions'''

# function to create invoice-city pair dictionary
def make_invoice_city_dict(df,city_col="City"):
    # create a dict of invoice number and city
    inv_df = df.copy()
    inv_df = inv_df.set_index('InvoiceNo')
    invoice_city_dict = inv_df.to_dict()[city_col]    
    
    # return the dictionary
    return invoice_city_dict


# function to convert the city names to their respective tiers
def city_tiers(x):
    x = x.title()
    if x in Tier1: return "Tier1"
    elif x in Tier2: return "Tier2"
    else: return 'Tier3'



# function to evaluate models
def evaluate_models(file_name, X, y, performance_metrics_df):

    # load the model
    model_path = init['all_models']['path']
    model = pickle.load(open(model_path+file_name, 'rb'))


    # calculate metrics
    performance_metrics_df.loc[performance_metrics_df['filename']==file_name,'accuracy_newdf'] = SCORERS['accuracy'](model, X, y)
    performance_metrics_df.loc[performance_metrics_df['filename']==file_name,'precision_newdf'] = SCORERS['precision'](model, X, y)
    performance_metrics_df.loc[performance_metrics_df['filename']==file_name,'recall_newdf'] = SCORERS['recall'](model, X, y)
    performance_metrics_df.loc[performance_metrics_df['filename']==file_name,'f1_newdf'] = SCORERS['f1'](model, X, y)


    # return the predict_performance_dict
    return performance_metrics_df




"""Main prediction function"""

# function to predict the output
def predict_all_models():
    
    # read the new data to predict
    df = pd.read_csv(init['divided_data']["new_df"]['path'])


    # read the prediction performance df
    performance_metrics_df = pd.read_csv(init['performance_data']["performance_metrics_df"]['path'])


    # initialize columns to save scores
    performance_metrics_df['accuracy_newdf'] = 0
    performance_metrics_df['precision_newdf'] = 0
    performance_metrics_df['recall_newdf'] = 0
    performance_metrics_df['f1_newdf'] = 0
    
    
    # convert cities into tiers
    df['City'] = df['City'].apply(city_tiers)
    df['DealerCity'] = df['DealerCity'].apply(city_tiers)
    
    
    # label encode the required columns
    for col in label_encode_cols:
        label_encoder = LabelEncoder()
        encoder_dict[col] = label_encoder.fit(df[col])
        df[col] = encoder_dict[col].transform(df[col])
       
    
    # standardize the required columns
    for col in standardize_cols:
        mmscaler = MinMaxScaler()
        encoder_dict[col] = mmscaler.fit(df[col].values.reshape(-1, 1))
        df[col] = encoder_dict[col].transform(df[col].values.reshape(-1, 1))


    # convert the columns to one hot encoded form
    ohe_encoder = OneHotEncoder(sparse=False) 

    ohe_classify_df = pd.DataFrame()

    for column in to_ohe:
        ohe_df = pd.DataFrame(ohe_encoder.fit_transform(df[column].values.reshape(-1,1)))
        ohe_df.columns = list(map(lambda x: column + "_" + x[3:],ohe_encoder.get_feature_names()))
        ohe_classify_df = pd.concat([ohe_classify_df,ohe_df],axis=1)
        
    df = pd.concat([ohe_classify_df,df[not_to_ohe+no_classification_cols]],axis=1)



    # Set X and y for testing
    y = df['LoanTaken']
    X = df.drop('LoanTaken',axis=1)


    # Redefine classification cols after one hot encoding
    classification_cols = X.columns[~X.columns.isin(no_classification_cols)]

    print(classification_cols)

    # calculate performance metrics for all models
    # list all the models
    file_name_list = performance_metrics_df['filename']
    for file_name in file_name_list:
        performance_metrics_df = evaluate_models(file_name,X[classification_cols],y,performance_metrics_df)
    

    # sort the dataframe in descending order with respect to f1_test
    performance_metrics_df = performance_metrics_df.sort_values(['f1_newdf'],ascending=[False])


    # save the performance metrics df
    performance_metrics_df.to_csv(init['performance_data']["performance_metrics_df"]['path'],index=False)

