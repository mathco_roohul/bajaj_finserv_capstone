# import required dataframes
import json
import pandas as pd
import numpy as np
import sys
import os
import pickle
from datetime import datetime
import eli5

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler

# import model evaluation library
from sklearn.metrics import SCORERS

# class to predict data
class Customer:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            # with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
            with open('init2.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
            self.classification_cols = self.init['columns']['classification']
            self.label_encode_cols = self.init['columns']['label_encode']
            self.standardize_cols = self.init['columns']['standardize']
            self.Tier1 = self.init['tiers']['tier1']
            self.Tier2 = self.init['tiers']['tier2']
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed init argument is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to create invoice-city pair dictionary
    def make_invoice_city_dict(self,df,city_col="City"):
        # create a dict of invoice number and city
        try:
            self.inv_df = df.copy()
            self.inv_df = self.inv_df.set_index('InvoiceNo')
            self.invoice_city_dict = self.inv_df.to_dict()[city_col]    
            # return the dictionary
            return self.invoice_city_dict
        except Exception as e:
            a = "\tLocation: Making invoice-city dictionary.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="make_invoice_city_dict")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to convert the city names to their respective tiers
    def city_tiers(self,city):
        try:
            self.city = str(city).title()
            if self.city in self.Tier1: return "Tier1"
            elif self.city in self.Tier2: return "Tier2"
            else: return 'Tier3'
        except Exception as e:
            a = "\tLocation: City to tier conversion.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            # self.log_errors(error=e,function_name="city_tiers")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to label encode columns
    def label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.le_df = df.copy()
            # create a dict to store all the fitted encoders
            self.label_encoder_dict = dict()
            # label encode the required columns
            for col in self.label_encode_cols:
                self.label_encoder = LabelEncoder()
                self.label_encoder_dict[col] = self.label_encoder.fit(self.le_df[col])
                self.le_df[col] = self.label_encoder_dict[col].transform(self.le_df[col])
            # return the label encoded dataframe
            return self.le_df
        except Exception as e:
            a = "\tLocation: Label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse label encode columns
    def inv_label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_le_df = df.copy()
            # inverse label encode the required columns
            for col in self.label_encode_cols:
                self.inv_le_df[col] = self.label_encoder_dict[col].inverse_transform(self.inv_le_df[col].astype(int))
            # return the label encoded dataframe
            return self.inv_le_df
        except Exception as e:
            a = "\tLocation: Inverse label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to standardize columns
    def standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.stan_df = df.copy()
            # create a dict to store all the fitted encoders
            self.standardize_encoder_dict = dict()
            # standardize the required columns
            for col in self.standardize_cols:
                self.mmscaler = MinMaxScaler()
                self.standardize_encoder_dict[col] = self.mmscaler.fit(self.stan_df[col].values.reshape(-1, 1))
                self.stan_df[col] = self.standardize_encoder_dict[col].transform(self.stan_df[col].values.reshape(-1, 1))
            # return the standardized dataframe
            return self.stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="standardize_df")
            return False


# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse standardize columns
    def inv_standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_stan_df = df.copy()
            # inverse standardize the required columns
            for col in self.standardize_cols:
                self.inv_stan_df[col] = self.standardize_encoder_dict[col].inverse_transform(self.inv_stan_df[col].values.reshape(-1,1))
            # return the label encoded dataframe
            return self.inv_stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to inverse standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_standardize_df")
            return False


# ----------------------------------------------------------------------------------------------------------------------

    # function to extract important features
    def extract_imp_features(self, model):
        try:
            # calculate feature importance of each feature
            self.feat_dict = {} # a dict to hold feature_name: feature_importance
            for feature, importance in zip(self.classification_cols, model.feature_importances_):
                self.feat_dict[feature] = importance 
            # sort the sictionary with respect to features importance value
            self.feat_dict = {column: importance for column, importance in sorted(self.feat_dict.items(), key=lambda item: item[1], reverse=True)}
            # convert decimal score to percentage for feature importance
            for feature, importance in self.feat_dict.items():
                self.feat_dict[feature] = str(round(importance*100,2))+"%"
            # return the feature importance dict
            return self.feat_dict
        except Exception as e:
            a = "\tLocation: Extracting important features.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="extract_imp_features")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to extract performance scores of the prediction model
    def prediction_scores(self,model,X,y):
        try:
            # create a scores dict
            self.scores_dict = dict()
            # calculate performance metrics for the model
            self.scores_dict["accuracy"] = SCORERS['accuracy'](model, X[self.classification_cols], y)
            self.scores_dict["precision"] = SCORERS['precision'](model, X[self.classification_cols], y)
            self.scores_dict["recall"] = SCORERS['recall'](model, X[self.classification_cols], y)
            self.scores_dict["f1_score"] = SCORERS['f1'](model, X[self.classification_cols], y)
            # return the scores dict
            return self.scores_dict
        except Exception as e:
            a = "\tLocation: Calculating prediction scores.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="prediction_scores")
            return False

    
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to predict the output
    def customer_data(self,merged_df,model,Invoice_No=None, Customer_Code=None):

        self.merged_df = merged_df
        self.model = model
        #print(self.merged_df)

        
        # create a dict of invoice number and city
        try:
            if type(self.make_invoice_city_dict(self.merged_df,city_col='City')) == bool:
                e = "There is a problem in make_invoice_city_dict, check its log to know error"
                a = "\tLocation: Creating dict of invoice number and city.\n"
                b = "\tProblem: "
                e = a+b+str(e)
                self.log_errors(error=e,function_name="predict_data")
                return False
            self.invoice_city_dict = self.make_invoice_city_dict(self.merged_df,city_col='City')
            self.invoice_dlcity_dict = self.make_invoice_city_dict(self.merged_df,city_col='DealerCity')
        except Exception as e:
            a = "\tLocation: Creating dict of invoice number and city.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="customer_data")
            return False
        
        # convert cities into tiers
        try:
            self.merged_df['City'] = self.merged_df['City'].apply(self.city_tiers)
            if False in self.merged_df['City'].values:
                e = "There is a problem in city_tiers, check its log to know error"
                a = "\tLocation: Creating dict of invoice number and city.\n"
                b = "\tProblem: "
                e = a+b+str(e)
                self.log_errors(error=e,function_name="customer_data")
                return False
            self.merged_df['DealerCity'] = self.merged_df['DealerCity'].apply(self.city_tiers)
            if False in self.merged_df['DealerCity'].values:
                e = "There is a problem in city_tiers, check its log to know error"
                a = "\tLocation: Creating dict of invoice number and city.\n"
                b = "\tProblem: "
                e = a+b+str(e)
                self.log_errors(error=e,function_name="customer_data")
                return False
        except Exception as e:
            a = "\tLocation: Converting cities to tiers.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="customer_data")
            return False
        
        # Label encode the data
        try:
            if type(self.label_encode_df(self.merged_df)) == bool:
                e = "There is a problem in label_encode_df function, check its log to know error"
                a = "\tLocation: Label encoding columns.\n"
                b = "\tProblem: "
                e = a+b+str(e)
                self.log_errors(error=e,function_name="customer_data")
                return False
            self.merged_df = self.label_encode_df(self.merged_df)
        except Exception as e:
            a = "\tLocation: Label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="customer_data")
            return False

        # Standardize the data
        try:
            if type(self.standardize_df(self.merged_df)) == bool:
                e = "There is a problem in standardize_df function, check its log to know error"
                a = "\tLocation: Standardizing columns.\n"
                b = "\tProblem: "
                e = a+b+str(e)
                self.log_errors(error=e,function_name="customer_data")
                return False
            self.merged_df = self.standardize_df(self.merged_df)
        except Exception as e:
            a = "\tLocation: Standardizing columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="customer_data")
            return False

        
        # select df with classification columns to explain prediction 
        if Customer_Code == None:
            self.explain_row = self.merged_df[self.merged_df['InvoiceNo']==Invoice_No].index
        elif Invoice_No == None:
            self.explain_row = self.merged_df[self.merged_df['CustomerCode']==Customer_Code].index

        # explain the prediction
        self.model_explanation = eli5.explain_prediction_df(self.model, self.merged_df.iloc[self.explain_row][self.classification_cols], feature_names = self.classification_cols) 

        # drop the bias row
        self.index_to_drop = self.model_explanation[self.model_explanation['feature']=='<BIAS>'].index
        self.model_explanation = self.model_explanation.drop(index=self.index_to_drop)[['feature','weight']]   

        # calculate the contribution of each feature
        self.weights = self.model_explanation['weight'].values
        self.contribution = self.weights/self.weights.sum(axis=0,keepdims=1)
        self.model_explanation['contribution'] = self.contribution   

        # customer_data
        self.customer_row = self.merged_df.iloc[self.explain_row].T.to_dict()[self.explain_row[0]]

        # top 5 features
        self.model_explanation = self.model_explanation.set_index('feature').head()
        self.top_5_features = self.model_explanation.to_dict()['contribution']
        for feature, contribution in self.top_5_features.items():
            self.top_5_features[feature] = str(round(self.top_5_features[feature]*100,2))+"%"
        print(self.customer_row)
        print(self.top_5_features)


        # log the customer information
        self.customer_row['InvoiceDate'] = str(self.customer_row['InvoiceDate'])
        self.customer_log_path = self.init['customer_log']['path']
        with open(self.customer_log_path,'w+') as customer_log_file:
            customer_log_file.write("Customer information:\n")
            json.dump(self.customer_row,customer_log_file,indent=2)
            customer_log_file.write("\n\n")
            customer_log_file.write("Top 5 factors driving the customer:\n")
            json.dump(self.top_5_features,customer_log_file,indent=2)      
        
# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Predict", script_name="PredictData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
