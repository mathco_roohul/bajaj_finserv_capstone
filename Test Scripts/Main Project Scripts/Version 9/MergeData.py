# import required dataframes
import json
import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime
import connection_to_db
import insert_data_to_db
import get_data_from_db
import create_and_drop_db

# class to merge data
class Merge:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            # with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
            with open('init2.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed filename or filetype is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to make InvoiceNo column unique
    def unique_invoice(self,df):

        try:
            # make a copy of df to work
            self.uniq_in_df = df.copy()

            # sort dataframe wrt InvoiceNo column 
            self.uniq_in_df = self.uniq_in_df.sort_values(['InvoiceNo'])

            # Generate cumulative count for each InvoiceNo
            self.uniq_in_df['Invoice_count'] = self.uniq_in_df.groupby(['InvoiceNo']).cumcount() + 1

            # Add that cumulative count for InvoiceNo to dataframe
            self.uniq_in_df['Concat_invoice'] = self.uniq_in_df['InvoiceNo'] + "_" + self.uniq_in_df['Invoice_count'].astype(str)

            # Replace InvoiceNo with modifiied InvoiceNo
            self.uniq_in_df = self.uniq_in_df.drop(['InvoiceNo','Invoice_count'],axis=1)
            self.uniq_in_df = self.uniq_in_df.rename(columns={'Concat_invoice':'InvoiceNo'})

            # return the df with unique InvoiceNo
            return self.uniq_in_df

        except Exception as e:
            a = "\tLocation: Unique Invoice Function.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="unique_invoice")
            return False

# ----------------------------------------------------------------------------------------------------------------------


    # function to merge retail and enquiry datasets
    def merge_retail_enquiry(self, retail_df, enquiry_df):
        self.retail_df = retail_df
        self.enquiry_df = enquiry_df
        # merge the two datasets
        try:
            self.merged_df = pd.merge(self.retail_df, self.enquiry_df, how = "left", on = ["Enquiry","State","ModelCode"])
        except Exception as e:
            a = "\tLocation: Merging data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

         # combine BranchType columns
        try:
            self.merged_df['BranchType'] = np.where(self.merged_df['BranchType_y'].isnull(), self.merged_df['BranchType_x'], self.merged_df['BranchType_y'])
            self.merged_df = self.merged_df.drop(columns = ['BranchType_x', 'BranchType_y'])
            self.merged_df.loc[self.merged_df['BranchType']!="Urban",'BranchType'] = 'Rural'
        except Exception as e:
            a = "\tLocation: Combining branch type columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine City columns
        try:
            self.merged_df['City'] = np.where(self.merged_df['City_y'].isnull(), self.merged_df['City_x'], self.merged_df['City_y'])
            self.merged_df = self.merged_df.drop(columns = ['City_x', 'City_y'])
        except Exception as e:
            a = "\tLocation: Combining city columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine DealerCity columns
        try:
            self.merged_df['DealerCity'] = np.where(self.merged_df['DealerCity_y'].isnull(), self.merged_df['DealerCity_x'], self.merged_df['DealerCity_y'])
            self.merged_df = self.merged_df.drop(columns = ['DealerCity_x', 'DealerCity_y'])
        except Exception as e:
            a = "\tLocation: Combining DealerCity columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine TotalAmount columns
        try:
            self.merged_df['TotalAmount'] = np.where(self.merged_df['TotalAmount_y'].isnull(), self.merged_df['TotalAmount_x'], self.merged_df['TotalAmount_y'])
            self.merged_df = self.merged_df.drop(columns = ['TotalAmount_x', 'TotalAmount_y'])
        except Exception as e:
            a = "\tLocation: Combining TotalAmount columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # combine ModelFamily columns
        try:
            self.merged_df['ModelFamily'] = np.where(self.merged_df['ModelFamily_y'].isnull(), self.merged_df['ModelFamily_x'], self.merged_df['ModelFamily_y'])
            self.merged_df = self.merged_df.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])
            self.ModelFamilyInclude = ['Splendor', 'Star city', 'Intruder', 'CBZ', 'Discover', 'RE', 'Boxer']
            self.merged_df.loc[~self.merged_df['ModelFamily'].isin(self.ModelFamilyInclude),'ModelFamily'] = 'Apache'
        except Exception as e:
            a = "\tLocation: Combining model family columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # fill null values
        try:
            self.merged_df['LeadType'] = self.merged_df['LeadType'].fillna("No Enquiry")
            self.merged_df['TestRideOffered'] = self.merged_df['TestRideOffered'].fillna("No Enquiry")
            self.merged_df['FollowUp'] = self.merged_df['FollowUp'].fillna("No Enquiry")
            self.merged_df['SourceOfEnquiry'] = self.merged_df['SourceOfEnquiry'].fillna("No Enquiry")
        except Exception as e:
            a = "\tLocation: Filling null values.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # clean LeadType column
        try:
            self.LeadTypeInclude = ['Walk In','No Enquiry', 'Telephone', 'Activity/Outdoor']
            self.merged_df.loc[~self.merged_df['LeadType'].isin(self.LeadTypeInclude),'LeadType'] = 'Others'
        except Exception as e:
            a = "\tLocation: Replacing excess calsses in LeadType to others.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # clean TestRideOffered column
        try:
            self.TestRideOfferedInclude = ['No Enquiry', 'Test Ride Taken', 'Customer Declined']
            self.merged_df.loc[~self.merged_df['TestRideOffered'].isin(self.TestRideOfferedInclude),'TestRideOffered'] = 'No'
        except Exception as e:
            a = "\tLocation: Replacing excess calsses in TestRideOffered to No.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False


        # clean FollowUp column
        try:
            self.FollowUpInclude = ['1st Follow up', 'No Enquiry', '2nd Follow up', '3rd Follow up']
            self.merged_df.loc[~self.merged_df['FollowUp'].isin(self.FollowUpInclude),'FollowUp'] = 'General Follow up'
        except Exception as e:
            a = "\tLocation: Replacing excess calsses in FollowUp to General Follow up.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False


        # clean SourceOfEnquiry column
        try:
            self.SourceOfEnquiryInclude = ['Source Not Available', 'No Enquiry', 'General Enquiry', 'Referral']
            self.merged_df.loc[~self.merged_df['SourceOfEnquiry'].isin(self.SourceOfEnquiryInclude),'SourceOfEnquiry'] = 'Others'
        except Exception as e:
            a = "\tLocation: Replacing excess calsses in SourceOfEnquiry to others.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # drop duplicate rows
        try:
            self.merged_df = self.merged_df.groupby(['Enquiry','State','ModelCode']).first().reset_index()
            self.merged_df = self.merged_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False

        # drop null rows
        try:
            self.merged_df = self.merged_df.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Dropping null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_retail_enquiry")
            return False


        return self.merged_df

# ----------------------------------------------------------------------------------------------------------------------


    # function to merge merged dataset and finance dataset
    def merge_finance(self, retail_df, enquiry_df, finance_df):

        self.finance_df = finance_df
        
        # read merged data and date
        try:
            self.merged_df = self.merge_retail_enquiry(retail_df, enquiry_df)
        except Exception as e:
            a = "\tLocation: Reading Merged Data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        
        # merge the two datasets
        try:
            self.merged_df_finance = pd.merge(self.merged_df, self.finance_df, how = "left", on = "ChassisNo")
        except Exception as e:
            a = "\tLocation: Merging data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # combine CustomerCode column and CustomerID column
        try:
            self.merged_df_finance['Customer_ID'] = np.where(self.merged_df_finance['CustomerID'].isnull(), self.merged_df_finance['CustomerCode'], self.merged_df_finance['CustomerID'])
            self.merged_df_finance = self.merged_df_finance.drop(columns = ['CustomerCode', 'CustomerID'])
            self.merged_df_finance = self.merged_df_finance.rename(columns= {'Customer_ID': 'CustomerCode'})
        except Exception as e:
            a = "\tLocation: Combining customer code and id columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # combine ModelFamily columns
        try:
            self.merged_df_finance['ModelFamily'] = np.where(self.merged_df_finance['ModelFamily_x'].isnull(), self.merged_df_finance['ModelFamily_y'], self.merged_df_finance['ModelFamily_x'])
            self.merged_df_finance = self.merged_df_finance.drop(columns = ['ModelFamily_x', 'ModelFamily_y'])
        except Exception as e:
            a = "\tLocation: Combining model family columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # convert LoanAmount column into a flag
        try:
            self.merged_df_finance['LoanTaken'] = np.where(self.merged_df_finance['LoanAmount'].isna(),0,1)
            self.merged_df_finance = self.merged_df_finance.drop('LoanAmount', axis=1)
        except Exception as e:
            a = "\tLocation: Converting loan amount column into flag.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # drop duplicate rows
        try:
            self.merged_df_finance = self.merged_df_finance.groupby(['Enquiry','State','ModelCode']).first().reset_index()
            self.merged_df_finance = self.merged_df_finance.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False

        # drop null rows
        try:
            self.merged_df_finance = self.merged_df_finance.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Dropping null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="merge_finance")
            return False


        # if all worked well, return merged_df_finance
        return self.merged_df_finance


# ----------------------------------------------------------------------------------------------------------------------

    # logging errors function
    def log_errors(self, error, function_name, class_name="Merge", script_name="MergeData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
