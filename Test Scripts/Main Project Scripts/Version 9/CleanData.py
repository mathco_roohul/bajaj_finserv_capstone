# import required dataframes
import json
import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime

# class to clean data
class Clean:
    
    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            # with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
            with open('init2.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed filename or filetype is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to extract numerical pincodes
    def extract_pin(self,individual_pin):
        try:
            self.individual_ext_pin = float(individual_pin)
        except:
            self.individual_ext_pin = np.nan
        return self.individual_ext_pin

# ----------------------------------------------------------------------------------------------------------------------

    # function to clean InvoiceDate column
    def clean_dates(self,individual_date):
        try:
            self.individual_date = pd.to_datetime(individual_date)
            if self.individual_date.date() == pd.to_datetime('today').date():
                self.individual_date = np.nan
        except:
            self.individual_date = np.nan
        return self.individual_date

# ----------------------------------------------------------------------------------------------------------------------


    # clean TotalAmount column
    def clean_total_amount(self, df, high_quantile=0.90, low_quantile=0.01):

        # try the code block below, and fill the error log if a problem arises
        try:
            # make a copy of df
            self.tot_amt_df = df.copy()

            # Get a list of medians for all model families
            self.price_median_dict = self.tot_amt_df.groupby('ModelFamily')['TotalAmount'].median().to_dict()

            # Extract 90% quantile value for each model family
            self.price_high_quantile_dict = dict()
            for model in self.price_median_dict.keys():
                self.price_high_quantile_dict[model] = self.tot_amt_df[self.tot_amt_df['ModelFamily']==model]['TotalAmount'].quantile(high_quantile)
                
            # Extract 1% quantile value for each model family
            self.price_low_quantile_dict = dict()
            for model in self.price_median_dict.keys():
                self.price_low_quantile_dict[model] = self.tot_amt_df[self.tot_amt_df['ModelFamily']==model]['TotalAmount'].quantile(low_quantile)

            # Change all upper quantile values to median
            for model in self.price_median_dict.keys():
                self.tot_amt_df.loc[(self.tot_amt_df['ModelFamily']==model) & 
                        (self.tot_amt_df['TotalAmount'] > self.price_high_quantile_dict[model]),'TotalAmount'] = self.price_median_dict[model]

            # Change all lower quantile values to median
            for model in self.price_median_dict.keys():
                self.tot_amt_df.loc[(self.tot_amt_df['ModelFamily']==model) & 
                        (self.tot_amt_df['TotalAmount'] < self.price_low_quantile_dict[model]),'TotalAmount'] = self.price_median_dict[model]
            # return the df with cleaned dataframe
            return self.tot_amt_df

        except Exception as e:
            a = "\tLocation: Cleaning total amount function.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_total_amount")
            return False


# ----------------------------------------------------------------------------------------------------------------------
    # function to clean retail data
    def clean_retail_data(self, retail_df):
        self.retail_df = retail_df
        # select the required columns
        try:
            self.retail_columns = self.init["original_data"]["retail_df"]["columns"]
        except Exception as e:
            a = "\tLocation: Reading column names.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        #Check of all the column names match the original dataset
        try:
            self.retail_df = self.retail_df[self.retail_columns]
        except IndexError:
            e = "There is no file in the retail data directory, please check the folder and run the code again"
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        except KeyError:
            e = "The folder/file name for retail_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        except ValueError:
            e = "The date in the filename is not properly formatted, fix it and try again. Format = yyyymmdd_filename.csv"
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="check_retail_data")
        except Exception as e:
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False


        # drop duplicate rows
        try:
            self.retail_df = self.retail_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        

        # clean Enquiry column from enquiry_df
        try:
            self.retail_df['Enquiry'] = np.where(self.retail_df["Enquiry"].str.isalnum(),self.retail_df['Enquiry'],np.nan)
        except Exception as e:
            a = "\tLocation: Cleaning Enquiry column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        # convert cochin to kochi in retail and enquiry datasets
        try:
            self.retail_df['DealerCity'] = self.retail_df['DealerCity'].str.replace("COCHIN","KOCHI").tolist()
        except Exception as e:
            a = "\tLocation: Cleaning DealerCity column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        
        # change column name in retail from DealerState to State
        try:
            self.retail_df = self.retail_df.rename(columns={"DealerState": "State"})
        except Exception as e:
            a = "\tLocation: Renaming Dealer state to state column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        
        # clean the pincode column
        try:
            self.retail_df['Pin'] = self.retail_df['Pin'].apply(self.extract_pin)
        except Exception as e:
            a = "\tLocation: Cleaning pin column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        
        # clean city and state names in retail and enquiry datasets
        try:
            self.pincode_df = pd.read_csv(self.init["supplimentary_data"]["pincode_df"]["path"])
            self.pincode_df = self.pincode_df.set_index('Pin')
            self.pin_city_dict = self.pincode_df.to_dict()['City']
            self.pin_state_dict = self.pincode_df.to_dict()['State']
            self.retail_df['City'] = self.retail_df['Pin'].map(self.pin_city_dict)
            self.retail_df['State'] = self.retail_df['Pin'].map(self.pin_state_dict)
            self.retail_df = self.retail_df.drop('Pin', axis=1)
        except KeyError:
            e = "The folder/file name for pincode_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Cleaning city and state with pin.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        except Exception as e:
            a = "\tLocation: Cleaning city and state with pin.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        

        # convert City, DealerCity, State to title case
        try:
            self.retail_df['City'] = self.retail_df['City'].str.title()
            self.retail_df['DealerCity'] = self.retail_df['DealerCity'].str.title()
            self.retail_df['State'] = self.retail_df['State'].str.title()
        except Exception as e:
            a = "\tLocation: Converting City, DealerCity, State to title case.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        # clean branch type column in retail and finance datasets
        try:
            self.city_branch_df = pd.read_csv(self.init["supplimentary_data"]["city_branch_df"]["path"])
            self.city_branch_df = self.city_branch_df.set_index("DealerCity")
            self.city_branch_dict = self.city_branch_df.to_dict()['BranchType']
            self.retail_df['BranchType'] = self.retail_df['DealerCity'].map(self.city_branch_dict)
        except KeyError:
            e = "The folder/file name for city_branch_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Cleaning BranchType column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        except Exception as e:
            a = "\tLocation: Cleaning BranchType column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        # clean TotalAmount column from retail and enquiry datasets
        try:
            if type(self.clean_total_amount(self.retail_df)) == bool:
                return False
            self.retail_df = self.clean_total_amount(self.retail_df)
        except Exception as e:
            a = "\tLocation: Cleaning Total Amount column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False


        # clean ChassisNo column from retail and finance datasets
        try:
            self.retail_df['ChassisNo'] = self.retail_df['ChassisNo'].astype(str)
            self.retail_df = self.retail_df[self.retail_df['ChassisNo'].map(len) == 17]
        except Exception as e:
            a = "\tLocation: Cleaning ChassisNo column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        
        # clean Segment column from retail_df
        try:
            self.retail_df = self.retail_df[self.retail_df['Segment'].isin(['M1','M2','M3','S1','S2'])]
        except Exception as e:
            a = "\tLocation: Cleaning Segment column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        
        # make Enquiry flag and Booking flag in retail_df
        try:
            self.retail_df['Enquiry_flag'] = np.where(self.retail_df['Enquiry'].isna(),0,1)
            self.retail_df['Booking_flag'] = np.where(self.retail_df['BookingNo_x'].isna(),0,1)
            self.retail_df = self.retail_df.drop("BookingNo_x",axis=1)
        except Exception as e:
            a = "\tLocation: Creating flags.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
        

        # Clean InvoiceDate column
        try:
            self.retail_df['InvoiceDate'] = self.retail_df['InvoiceDate'].apply(self.clean_dates)
        except Exception as e:
            a = "\tLocation: Cleaning Invoice Date column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        # drop all rows with null values
        try:
            self.retail_df = self.retail_df.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Drop all null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False

        # drop duplicate rows once more
        try:
            self.retail_df = self.retail_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Drop all duplicate rows at end.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_retail_data")
            return False
                
        return self.retail_df

# ----------------------------------------------------------------------------------------------------------------------


    # function to clean enquiry data
    def clean_enquiry_data(self, enquiry_df):
        self.enquiry_df = enquiry_df
        # select the required columns
        try:
            self.enquiry_columns = self.init["original_data"]["enquiry_df"]["columns"]
        except Exception as e:
            a = "\tLocation: Reading enquiry columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        #Check all column names with original data
        try:
            self.enquiry_df = self.enquiry_df[self.enquiry_columns]
        except Exception as e:
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
        
        
        # drop duplicate rows
        try:
            self.enquiry_df = self.enquiry_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # change column name in enquiry from DocName to Enquiry
        try:
            self.enquiry_df = self.enquiry_df.rename(columns={"DocName": "Enquiry"})
        except Exception as e:
            a = "\tLocation: Changing column name in enquiry from DocName to Enquiry.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
        
        
        # clean Enquiry column from enquiry_df
        try:
            self.enquiry_df['Enquiry'] = np.where(self.enquiry_df["Enquiry"].str.isalnum(),self.enquiry_df['Enquiry'],np.nan)
        except Exception as e:
            a = "\tLocation: Cleaning Enquiry column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # convert cochin to kochi in retail and enquiry datasets
        try:
            self.enquiry_df['DealerCity'] = self.enquiry_df['DealerCity'].str.replace("COCHIN","KOCHI").tolist()
        except Exception as e:
            a = "\tLocation: Cleaning DealerCity column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # clean the pincode column
        try:
            self.enquiry_df['Pin'] = self.enquiry_df['Pin'].apply(self.extract_pin)
        except Exception as e:
            a = "\tLocation: Cleaning Pin column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

         # clean city and state names in retail and enquiry datasets
        try:
            self.pincode_df = pd.read_csv(self.init["supplimentary_data"]["pincode_df"]["path"])
            self.pincode_df = self.pincode_df.set_index('Pin')
            self.pin_city_dict = self.pincode_df.to_dict()['City']
            self.pin_state_dict = self.pincode_df.to_dict()['State']
            self.enquiry_df['City'] = self.enquiry_df['Pin'].map(self.pin_city_dict)
            self.enquiry_df['State'] = self.enquiry_df['Pin'].map(self.pin_state_dict)
            self.enquiry_df = self.enquiry_df.drop('Pin', axis=1)
        except KeyError:
            e = "The folder/file name for pincode_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Cleaning city and state columns from pin column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
        except Exception as e:
            a = "\tLocation: Cleaning city and state columns from pin column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # convert City, DealerCity, State to title case
        try:
            self.enquiry_df['City'] = self.enquiry_df['City'].str.title()
            self.enquiry_df['DealerCity'] = self.enquiry_df['DealerCity'].str.title()
            self.enquiry_df['State'] = self.enquiry_df['State'].str.title()
        except Exception as e:
            a = "\tLocation: Converting City, DealerCity, State to title case.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # clean branch type column in retail and finance datasets
        try:
            self.city_branch_df = pd.read_csv(self.init["supplimentary_data"]["city_branch_df"]["path"])
            self.city_branch_df = self.city_branch_df.set_index("DealerCity")
            self.city_branch_dict = self.city_branch_df.to_dict()['BranchType']
            self.enquiry_df['BranchType'] = self.enquiry_df['DealerCity'].map(self.city_branch_dict)
        except KeyError:
            e = "The folder/file name for city_branch_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Cleaning Branch Type column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
        except Exception as e:
            a = "\tLocation: Cleaning Branch Type column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # clean TotalAmount column from retail and enquiry datasets
        try:
            if type(self.clean_total_amount(self.enquiry_df)) == bool:
                return False
            self.enquiry_df = self.clean_total_amount(self.enquiry_df)
        except Exception as e:
            a = "\tLocation: Cleaning Total Amount column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
            

        # clean TestRideOffered column from enquiry_df
        try:
            self.enquiry_df['TestRideOffered'] = self.enquiry_df['TestRideOffered'].fillna("No")
        except Exception as e:
            a = "\tLocation: Cleaning TestRideOffered column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # clean SourceOfEnquiry column from enquiry_df
        try:
            self.enquiry_df['SourceOfEnquiry'] = self.enquiry_df['SourceOfEnquiry'].fillna("Source Not Available")
        except Exception as e:
            a = "\tLocation: Cleaning Source of enquiry column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False


        # drop all rows with null values
        try:
            self.enquiry_df = self.enquiry_df.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Dropping all null rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False

        # drop duplicate rows once more
        try:
            self.enquiry_df = self.enquiry_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows at the end.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_enquiry_data")
            return False
                
        return self.enquiry_df

# ----------------------------------------------------------------------------------------------------------------------

    def clean_finance_data(self, finance_df):

        self.finance_df = finance_df

        # select the required columns
        try:
            self.finance_columns = self.init["original_data"]["finance_df"]["columns"]
        except Exception as e:
            a = "\tLocation: Reading column names.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        #Check all column names with original data
        try:
            self.finance_df = self.finance_df[self.finance_columns]
        except Exception as e:
            a = "\tLocation: Reading data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        
        # drop duplicate rows
        try:
            self.finance_df = self.finance_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Dropping duplicate rows.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        # clean ChassisNo column from retail and finance datasets
        try:
            self.finance_df['ChassisNo'] = self.finance_df['ChassisNo'].astype(str)
            self.finance_df = self.finance_df[self.finance_df['ChassisNo'].map(len) == 17]
        except Exception as e:
            a = "\tLocation: Cleaning ChassisNo column.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        # change column name in Finance_df from MAKE to ModelFamily
        try:
            self.finance_df = self.finance_df.rename(columns={"MAKE": "ModelFamily"})
            self.finance_df = self.finance_df.rename(columns={"AMTFIN": "LoanAmount"})
            self.finance_df = self.finance_df.rename(columns={"CUSTOMERID": "CustomerID"})
        except Exception as e:
            a = "\tLocation: Changing column names.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        # drop all rows with null values
        try:
            self.finance_df = self.finance_df.dropna(axis=0)
        except Exception as e:
            a = "\tLocation: Drop rows with null values.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False

        # drop duplicate rows once more
        try:
            self.finance_df = self.finance_df.drop_duplicates()
        except Exception as e:
            a = "\tLocation: Drop duplicate rows at end.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="clean_finance_data")
            return False
                

        # if all worked well, return Dataframe
        return self.finance_df

# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Clean", script_name="CleanData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
