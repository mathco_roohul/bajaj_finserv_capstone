import pandas as pd 
import connection_to_db as connection
import get_data_from_db
import insert_data_to_db
from datetime import datetime
from PredictData import Predict
from BlobConnect import Blob
import json
import io

with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
    init = json.load(init_file)
error_log_path = init['error_log']['path']
#--Error Logging------------------------------------------------------------

def log_errors(error, function_name, class_name="Predict", script_name="t_predict.py"):
    with open(error_log_path,'a+') as error_log_file:
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d_%H:%M:%S")
        error_log_file.write("Time: {}\n".format(dt_string))
        error_log_file.write("Script name: {}\n".format(script_name))
        error_log_file.write("Class name: {}\n".format(class_name))
        error_log_file.write("Function name: {}\n".format(function_name))
        error_log_file.write("Error: \n{}\n\n".format(error))

#--Predict function---------------------------------------------------------

def predict_data():

    #Extract merged dataset
    try:
        merged_df = get_data_from_db.get_merged_stg_data()
    except Exception as e:
        a = "\tLocation: Reading retail data.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="get_ret_stg_data")

    #Extract the model
    try:
        blob = Blob()
        model = blob.download_model("All_models/model.sav")
        print("Success")
    except Exception as e:
        a = "\tLocation: Reading enquiry data from blob.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="Read_data_from_blob")

    # predict the output
    predict = Predict()
    predicted_df, most_likely_predicted_df, maybe_predicted_df, least_likely_predicted_df = predict.predict_data(merged_df,model)

    # save the predicted_df dataframes to blob
    try:
        output = predicted_df.to_csv(index_label="idx", encoding = "utf-8", index=False)
        blob.upload_data("Prediction_data/Full_data/20201012_Full_Predicted_data.csv",output)
    except Exception as e:
        a = "\tLocation: Pushing predicted_df into the blob.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="push_data_to_blob")
    
    # save the most_likely_predicted_df dataframes to blob
    try:
        output = most_likely_predicted_df.to_csv(index_label="idx", encoding = "utf-8", index=False)
        blob.upload_data("Prediction_data/Most_likely_data/20201012_Most_likely_data.csv",output)
    except Exception as e:
        a = "\tLocation: Pushing most_likely_predicted_df into the blob.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="push_data_to_blob")

    # save the maybe_predicted_df dataframes to blob
    try:
        output = maybe_predicted_df.to_csv(index_label="idx", encoding = "utf-8", index=False)
        blob.upload_data("Prediction_data/Maybe_data/20201012_Maybe_data.csv",output)
    except Exception as e:
        a = "\tLocation: Pushing maybe_predicted_df into the blob.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="push_data_to_blob")

    # save the least_likely_predicted_df dataframes to blob
    try:
        output = least_likely_predicted_df.to_csv(index_label="idx", encoding = "utf-8", index=False)
        blob.upload_data("Prediction_data/Least_likely_data/20201012_Least_likely_data.csv",output)
    except Exception as e:
        a = "\tLocation: Pushing least_likely_predicted_df into the blob.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="push_data_to_blob")
    
    #save the data into the predicted_fin table
    try:
        insert_data_to_db.insert_pred_fin_table(predicted_df)
        print("Inserted predicted data into final table successfully.")
    except Exception as e:
        a = "\tLocation: Predicted Final Table Data Insert.\n"
        b = "\tProblem: "
        e = a+b+str(e)
        log_errors(error=e,function_name="insert_pred_fin_table")

predict_data()