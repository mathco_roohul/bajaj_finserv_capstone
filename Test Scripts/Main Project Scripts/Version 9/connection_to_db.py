import pyodbc
import pandas as pd

server = 'capstonesqlnew.database.windows.net,1433' # to specify an alternate port
database = 'Capstone_AzureSQL' 
username = 'Mathco' 
password = 'Math@1234$' 

def connect():
    try:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = cnxn.cursor()
    except Exception as e:
       	print(e)
       	print("Error in creating cursor")
    return cursor , cnxn

def close_conn(cnxn):
    try:
        cnxn.close()
    except Exception as e:
       	print(e)
        print("error in closing")       