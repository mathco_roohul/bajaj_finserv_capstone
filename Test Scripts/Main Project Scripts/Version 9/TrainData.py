'''Imports'''

# import required dataframes
import json
import pandas as pd
import numpy as np
from datetime import datetime

# import undersampling libraries
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import NeighbourhoodCleaningRule
from imblearn.under_sampling import RandomUnderSampler


# import oversampling libraries
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.over_sampling import ADASYN
from imblearn.pipeline import Pipeline

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

# import ML models
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.pipeline import make_pipeline, make_union
from sklearn.tree import DecisionTreeClassifier
from tpot.builtins import StackingEstimator
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold

# import model evaluation library
from sklearn.metrics import SCORERS
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix

# import model saving library
import pickle

# class to train the data
class Train:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            # with open('/home/azureuser/bajaj/capstone/init.json') as init_file:
            with open('init2.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
            self.classification_cols = self.init['columns']['classification']
            self.no_classification_cols = self.init['columns']['no_classification']
            self.OHE_cols  = self.init["columns"]['one_hot_encode']
            self.No_OHE_cols  = self.init["columns"]['not_one_hot_encode']
            self.OHE_out_cols = self.init['columns']['one_hot_encode_df_cols']
            self.label_encode_cols = self.init['columns']['label_encode']
            self.standardize_cols = self.init['columns']['standardize']
            self.Tier1 = self.init['tiers']['tier1']
            self.Tier2 = self.init['tiers']['tier2']
            self.Northern_region = self.init['regions']["Northern"]
            self.Southern_region = self.init['regions']["Southern"]
            self.Central_region = self.init['regions']["Central"]
            self.Western_region = self.init['regions']["Western"]
            self.Eastern_region = self.init['regions']["Eastern"]
            self.Northeastern_region = self.init['regions']["Northeastern"]
            self.Bay_Of_Bengal_region = self.init['regions']["Bay of Bengal"]
            self.Arabian_Sea_region = self.init['regions']["Arabian Sea"]
            self.encoder_dict = dict()

        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed init argument is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        

# ----------------------------------------------------------------------------------------------------------------------


    # function to convert the city names to their respective tiers
    def city_tiers(self,city):
        try:
            self.city = str(city).title()
            if self.city in self.Tier1: return "Tier1"
            elif self.city in self.Tier2: return "Tier2"
            else: return 'Tier3'
        except Exception as e:
            a = "\tLocation: City to tier conversion.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            # self.log_errors(error=e,function_name="city_tiers")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to convert the city names to their respective tiers
    def state_regions(self,state):
        self.state = str(state).title()
        if self.state in self.Northern_region: return "Northern"
        elif self.state in self.Southern_region: return "Southern"
        elif self.state in self.Central_region: return "Central"
        elif self.state in self.Western_region: return "Western"
        elif self.state in self.Eastern_region: return "Eastern"
        elif self.state in self.Northeastern_region: return "Northeastern"
        elif self.state in self.Bay_Of_Bengal_region: return "Bay_Of_Bengal"
        else: return 'Arabian_Sea'

# ----------------------------------------------------------------------------------------------------------------------

    # function to label encode columns
    def label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.le_df = df.copy()
            # create a dict to store all the fitted encoders
            self.label_encoder_dict = dict()
            # label encode the required columns
            for col in self.label_encode_cols:
                self.label_encoder = LabelEncoder()
                self.label_encoder_dict[col] = self.label_encoder.fit(self.le_df[col])
                self.le_df[col] = self.label_encoder_dict[col].transform(self.le_df[col])
            # return the label encoded dataframe
            return self.le_df
        except Exception as e:
            a = "\tLocation: Label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse label encode columns
    def inv_label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_le_df = df.copy()
            # inverse label encode the required columns
            for col in self.label_encode_cols:
                self.inv_le_df[col] = self.label_encoder_dict[col].inverse_transform(self.inv_le_df[col].astype(int))
            # return the label encoded dataframe
            return self.inv_le_df
        except Exception as e:
            a = "\tLocation: Inverse label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to standardize columns
    def standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.stan_df = df.copy()
            # create a dict to store all the fitted encoders
            self.standardize_encoder_dict = dict()
            # standardize the required columns
            for col in self.standardize_cols:
                self.mmscaler = MinMaxScaler()
                self.standardize_encoder_dict[col] = self.mmscaler.fit(self.stan_df[col].values.reshape(-1, 1))
                self.stan_df[col] = self.standardize_encoder_dict[col].transform(self.stan_df[col].values.reshape(-1, 1))
            # return the standardized dataframe
            return self.stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="standardize_df")
            return False


# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse standardize columns
    def inv_standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_stan_df = df.copy()
            # inverse standardize the required columns
            for col in self.standardize_cols:
                self.inv_stan_df[col] = self.standardize_encoder_dict[col].inverse_transform(self.inv_stan_df[col].values.reshape(-1,1))
            # return the label encoded dataframe
            return self.inv_stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to inverse standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_standardize_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to select sampler
    def select_sampler(self,num):
        
        if num == 0:
            return ("EditedNearestNeighbours", EditedNearestNeighbours())
        elif num == 1:
            return ("RandomUnderSampler", RandomUnderSampler())
        elif num == 2:
            return ("NearMiss", NearMiss(version=3))
        elif num == 3:
            return ( "TomekLinks", TomekLinks())
        elif num == 4:
            return ("OneSidedSelection", OneSidedSelection())
        elif num == 5:
            return ("NeighbourhoodCleaningRule", NeighbourhoodCleaningRule(n_neighbors=3, threshold_cleaning=0.5))
        elif num == 6:
            return ("SMOTE", SMOTE())
        elif num == 7:
            return ("BorderlineSMOTE", BorderlineSMOTE())
        elif num == 8:
            return ("ADASYN", ADASYN())

    # function to select classifier
    def select_classifier(self,num):

        if num == 0:
            return ("KNeighborsClassifier_1", KNeighborsClassifier(n_neighbors=62, p=1, weights="distance"))
        elif num == 1:
            return ("RandomForestClassifier_1", RandomForestClassifier(bootstrap=False, criterion="gini", max_features=0.2, min_samples_leaf=4, min_samples_split=2, n_estimators=100))        
        elif num == 2:
            return ("RandomForestClassifier_2", RandomForestClassifier(bootstrap=False, criterion="gini", max_features=0.15000000000000002, min_samples_leaf=2, min_samples_split=13, n_estimators=100))
        elif num == 3:
            return ("RandomForestClassifier_3", RandomForestClassifier())
        elif num == 4:
            return ("DecisionTreeClassifier_1", DecisionTreeClassifier())
        elif num == 5:
            return ("GradientBoostingClassifier_1", GradientBoostingClassifier())
        elif num == 6:
            exported_pipeline1 = make_pipeline(
                PCA(iterated_power=3, svd_solver="randomized"),
                StackingEstimator(estimator=DecisionTreeClassifier(criterion="entropy", max_depth=1, min_samples_leaf=17, min_samples_split=11)),
                KNeighborsClassifier(n_neighbors=2, p=2, weights="distance")
            )
            return ("Pipeline_1",exported_pipeline1)
        elif num == 7:
            exported_pipeline2 = make_pipeline(    
                StackingEstimator(estimator=DecisionTreeClassifier(criterion="gini", max_depth=8, min_samples_leaf=19, min_samples_split=3)),    
                VarianceThreshold(threshold=0.0005),    
                PCA(iterated_power=10, svd_solver="randomized"),    
                PCA(iterated_power=5, svd_solver="randomized"),    
                KNeighborsClassifier(n_neighbors=96, p=1, weights="distance")
            )
            return ("Pipeline_2",exported_pipeline2)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to perform OneHot Encoding
    def custom_one_hot_encoding(self, df):
           
        # Create a dataframe with zeroes
        self.OHE_out_df = pd.DataFrame(np.zeros((len(df), len(self.OHE_out_cols))), columns=self.OHE_out_cols)   
        
        # Fill the zeroes with 1s where ever condition is met
        try: self.OHE_out_df['Segment_M1'] = np.where(df['Segment']=='M1',1,0)
        except: pass
        try: self.OHE_out_df['Segment_M2'] = np.where(df['Segment']=='M2',1,0)
        except: pass
        try: self.OHE_out_df['Segment_M3'] = np.where(df['Segment']=='M3',1,0)
        except: pass
        try: self.OHE_out_df['Segment_S1'] = np.where(df['Segment']=='S1',1,0)
        except: pass
        try: self.OHE_out_df['Segment_S2'] = np.where(df['Segment']=='S2',1,0)
        except: pass
        try: self.OHE_out_df['LeadType_Walk In'] = np.where(df['LeadType']=='Walk In',1,0)
        except: pass
        try: self.OHE_out_df['LeadType_No Enquiry'] = np.where(df['LeadType']=='No Enquiry',1,0)
        except: pass
        try: self.OHE_out_df['LeadType_Telephone'] = np.where(df['LeadType']=='Telephone',1,0)
        except: pass
        try: self.OHE_out_df['LeadType_Activity/Outdoor'] = np.where(df['LeadType']=='Activity/Outdoor',1,0)
        except: pass
        try: self.OHE_out_df['LeadType_Others'] = np.where(df['LeadType']=='Others',1,0)
        except: pass
        try: self.OHE_out_df['TestRideOffered_No'] = np.where(df['TestRideOffered']=='No',1,0)
        except: pass
        try: self.OHE_out_df['TestRideOffered_No Enquiry'] = np.where(df['TestRideOffered']=='No Enquiry',1,0)
        except: pass
        try: self.OHE_out_df['TestRideOffered_Test Ride Taken'] = np.where(df['TestRideOffered']=='Test Ride Taken',1,0)
        except: pass
        try: self.OHE_out_df['TestRideOffered_Customer Declined'] = np.where(df['TestRideOffered']=='Customer Declined',1,0)
        except: pass
        try: self.OHE_out_df['FollowUp_1st Follow up'] = np.where(df['FollowUp']=='1st Follow up',1,0)
        except: pass
        try: self.OHE_out_df['FollowUp_2nd Follow up'] = np.where(df['FollowUp']=='2nd Follow up',1,0)
        except: pass
        try: self.OHE_out_df['FollowUp_3rd Follow up'] = np.where(df['FollowUp']=='3rd Follow up',1,0)
        except: pass
        try: self.OHE_out_df['FollowUp_General Follow up'] = np.where(df['FollowUp']=='General Follow up',1,0)
        except: pass
        try: self.OHE_out_df['FollowUp_No Enquiry'] = np.where(df['FollowUp']=='No Enquiry',1,0)
        except: pass
        try: self.OHE_out_df['SourceOfEnquiry_Source Not Available'] = np.where(df['SourceOfEnquiry']=='Source Not Available',1,0)
        except: pass
        try: self.OHE_out_df['SourceOfEnquiry_No Enquiry'] = np.where(df['SourceOfEnquiry']=='No Enquiry',1,0)
        except: pass
        try: self.OHE_out_df['SourceOfEnquiry_General Enquiry'] = np.where(df['SourceOfEnquiry']=='General Enquiry',1,0)
        except: pass
        try: self.OHE_out_df['SourceOfEnquiry_Others'] = np.where(df['SourceOfEnquiry']=='Others',1,0)
        except: pass
        try: self.OHE_out_df['SourceOfEnquiry_Referral'] = np.where(df['SourceOfEnquiry']=='Referral',1,0)
        except: pass
        try: self.OHE_out_df['BranchType_Urban'] = np.where(df['BranchType']=='Urban',1,0)
        except: pass
        try: self.OHE_out_df['BranchType_Rural'] = np.where(df['BranchType']=='Rural',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Apache'] = np.where(df['ModelFamily']=='Apache',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Splendor'] = np.where(df['ModelFamily']=='Splendor',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Star city'] = np.where(df['ModelFamily']=='Star city',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Intruder'] = np.where(df['ModelFamily']=='Intruder',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_CBZ'] = np.where(df['ModelFamily']=='CBZ',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Discover'] = np.where(df['ModelFamily']=='Discover',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_RE'] = np.where(df['ModelFamily']=='RE',1,0)
        except: pass
        try: self.OHE_out_df['ModelFamily_Boxer'] = np.where(df['ModelFamily']=='Boxer',1,0)
        except: pass
        try: self.OHE_out_df['City_Tier1'] = np.where(df['City']=='Tier1',1,0)
        except: pass
        try: self.OHE_out_df['City_Tier2'] = np.where(df['City']=='Tier2',1,0)
        except: pass
        try: self.OHE_out_df['City_Tier3'] = np.where(df['City']=='Tier3',1,0)
        except: pass
        try: self.OHE_out_df['DealerCity_Tier1'] = np.where(df['DealerCity']=='Tier1',1,0)
        except: pass
        try: self.OHE_out_df['DealerCity_Tier2'] = np.where(df['DealerCity']=='Tier2',1,0)
        except: pass
        try: self.OHE_out_df['DealerCity_Tier3'] = np.where(df['DealerCity']=='Tier3',1,0)
        except: pass
        try: self.OHE_out_df['State_Southern'] = np.where(df['State']=='Southern',1,0)
        except: pass
        try: self.OHE_out_df['State_Northern'] = np.where(df['State']=='Northern',1,0)
        except: pass
        try: self.OHE_out_df['State_Western'] = np.where(df['State']=='Western',1,0)
        except: pass
        try: self.OHE_out_df['State_Eastern'] = np.where(df['State']=='Eastern',1,0)
        except: pass
        try: self.OHE_out_df['State_Central'] = np.where(df['State']=='Central',1,0)
        except: pass
        try: self.OHE_out_df['State_Northeastern'] = np.where(df['State']=='Northeastern',1,0)
        except: pass
        try: self.OHE_out_df['State_Arabian_Sea'] = np.where(df['State']=='Arabian_Sea',1,0)
        except: pass
        try: self.OHE_out_df['State_Bay_Of_Bengal'] = np.where(df['State']=='Bay_Of_Bengal',1,0)
        except: pass
        

        # Return the dataframe
        return self.OHE_out_df


# ----------------------------------------------------------------------------------------------------------------------


    # function to evaluate the model
    def evaluate_model(self, sampling_name, model_name, model, Xtrain, ytrain, Xtest, ytest, Xnew, ynew):

        print("Performance calculation started")

        print("Important Features")
        # get importance
        importance = model.feature_importances_
        for i,v in zip(Xtrain.columns,importance):
            print('Feature: {}, Score: {}'.format(i,v))
                
        # fill the scores in the dictionary
        performance_dict = dict()
        performance_dict['Sampling'] = sampling_name
        performance_dict['Model'] = model_name

        # evaluate the model for training data
        # performance_dict['accuracy_train'] = SCORERS['accuracy'](model, Xtrain, ytrain)
        # performance_dict['precision_train'] = SCORERS['precision'](model, Xtrain, ytrain)
        # performance_dict['recall_train'] = SCORERS['recall'](model, Xtrain, ytrain)
        # performance_dict['f1_train'] = SCORERS['f1'](model, Xtrain, ytrain)
        ypred = model.predict(Xtrain)
        performance_dict['accuracy_train'] = accuracy_score(ytrain, ypred)
        performance_dict['precision_train'] = precision_score(ytrain, ypred)
        performance_dict['recall_train'] = recall_score(ytrain, ypred)
        performance_dict['f1_train'] = f1_score(ytrain, ypred)
            
        # evaluate the model for testing data
        # performance_dict['accuracy_test'] = SCORERS['accuracy'](model, Xtest, ytest)
        # performance_dict['precision_test'] = SCORERS['precision'](model, Xtest, ytest)
        # performance_dict['recall_test'] = SCORERS['recall'](model, Xtest, ytest)
        # performance_dict['f1_test'] = SCORERS['f1'](model, Xtest, ytest)
        ypred = model.predict(Xtest)
        performance_dict['accuracy_test'] = accuracy_score(ytest, ypred)
        performance_dict['precision_test'] = precision_score(ytest, ypred)
        performance_dict['recall_test'] = recall_score(ytest, ypred)
        performance_dict['f1_test'] = f1_score(ytest, ypred)

        # evaluate the model for new data
        # performance_dict['accuracy_new'] = SCORERS['accuracy'](model, Xnew, ynew)
        # performance_dict['precision_new'] = SCORERS['precision'](model, Xnew, ynew)
        # performance_dict['recall_new'] = SCORERS['recall'](model, Xnew, ynew)
        # performance_dict['f1_new'] = SCORERS['f1'](model, Xnew, ynew)
        ypred = model.predict(Xnew)
        performance_dict['accuracy_new'] = accuracy_score(ynew, ypred)
        performance_dict['precision_new'] = precision_score(ynew, ypred)
        performance_dict['recall_new'] = recall_score(ynew, ypred)
        performance_dict['f1_new'] = f1_score(ynew, ypred)

        # calculate confusion matrix
        cm_new = confusion_matrix(ynew, ypred, labels=[0,1])

        # return the scores
        return performance_dict, cm_new


# ----------------------------------------------------------------------------------------------------------------------


    # function to build models
    def train_model(self, original_main_df, original_new_df, sampler_num=1, model_num=1):

        # save the dataframes in class
        self.main_df = original_main_df.copy()
        self.new_df = original_new_df.copy()

        # convert the city names to their respective tiers
        self.main_df['City'] = self.main_df['City'].apply(self.city_tiers)
        self.main_df['DealerCity'] = self.main_df['DealerCity'].apply(self.city_tiers)
        self.new_df['City'] = self.new_df['City'].apply(self.city_tiers)
        self.new_df['DealerCity'] = self.new_df['DealerCity'].apply(self.city_tiers)

        # convert the state names to their respective state names
        self.main_df['State'] = self.main_df['State'].apply(self.state_regions)
        self.new_df['State'] = self.new_df['State'].apply(self.state_regions)

        # label encode the required columns of main_df
        for col in self.label_encode_cols:
            self.le = LabelEncoder()
            self.le = self.le.fit(self.main_df[col])
            self.main_df[col] = self.le.transform(self.main_df[col])

        # label encode the required columns of new_df
        for col in self.label_encode_cols:
            self.le = LabelEncoder()
            self.le = self.le.fit(self.new_df[col])
            self.new_df[col] = self.le.transform(self.new_df[col])

        # standardize the required columns in main_df
        for col in self.standardize_cols:
            self.mmscaler1 = MinMaxScaler()
            self.mmscaler1 = self.mmscaler1.fit(self.main_df[col].values.reshape(-1, 1))
            self.main_df[col] = self.mmscaler1.transform(self.main_df[col].values.reshape(-1, 1))

        # standardize the required columns in new_df
        for col in self.standardize_cols:
            self.mmscaler2 = MinMaxScaler()
            self.mmscaler2 = self.mmscaler2.fit(self.new_df[col].values.reshape(-1, 1))
            self.new_df[col] = self.mmscaler2.transform(self.new_df[col].values.reshape(-1, 1))

        # Set X and y for training
        self.y = self.main_df['LoanTaken']
        self.X = self.main_df.drop('LoanTaken',axis=1)


        # # classificaion columns after one hot encoding
        # self.classification_cols_ohe = self.X.columns[~self.X.columns.isin(self.no_classification_cols)]


        # split the data in equal proportions of 1s and 0s
        self.data_split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=100)
        for train_index, test_index in self.data_split.split(self.X, self.y):
            self.Xtrain, self.Xtest = self.X.iloc[train_index], self.X.iloc[test_index]
            self.ytrain, self.ytest = self.y.iloc[train_index], self.y.iloc[test_index]

        
        # sample the data
        self.sampler_name, self.sampler = self.select_sampler(sampler_num)
        self.Xtrain, self.ytrain = self.sampler.fit_resample(self.Xtrain[self.classification_cols],self.ytrain)


        # train the model
        self.model_name, self.model = self.select_classifier(model_num)
        self.model.fit(self.Xtrain[self.classification_cols], self.ytrain)

        # calculate performance metrics
        self.performance_dict, self.cm_new = self.evaluate_model(self.sampler_name, self.model_name, self.model, self.Xtrain[self.classification_cols], self.ytrain, self.Xtest[self.classification_cols], self.ytest, self.new_df[self.classification_cols], self.new_df['LoanTaken'])

        return (self.model, self.performance_dict, self.cm_new)

# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Predict", script_name="PredictData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
