import datetime
import logging
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import  BashOperator
from airflow.utils.dates import days_ago
import subprocess
import signal
import os
import json

def greet():
   print("Start Data Pipeline")

args = {
    "owner": "bajaj",
    "retries": 1,
    "start_date": days_ago(2),
}

dag = DAG('finserv_predict_pipeline_v2', default_args = args, tags = ['Bajaj Finserv'] , start_date = datetime.datetime.now())
venv_path = "/home/azureuser/bajaj/bajaj_env/bin/activate"
check_data_path = "/home/azureuser/bajaj/capstone/t_check_data.py"
clean_enquiry_path = "/home/azureuser/bajaj/capstone/t_clean_enquiry_v2.py"
clean_retail_path = "/home/azureuser/bajaj/capstone/t_clean_retail_v2.py"
merge_data_path = '/home/azureuser/bajaj/capstone/t_merge_data_v2.py'
upload_cleaned_retail_path = '/home/azureuser/bajaj/capstone/t_upload_clean_retail_data.py'
upload_cleaned_enquiry_path = '/home/azureuser/bajaj/capstone/t_upload_clean_enquiry_data.py'
upload_merged_data_path = '/home/azureuser/bajaj/capstone/t_upload_merged_data.py'
predict_path = '/home/azureuser/bajaj/capstone/t_predict_v2.py'
upload_predict_path = '/home/azureuser/bajaj/capstone/t_upload_predict_data.py'



def python_venv_command(path):

    command = ". " + venv_path + " &&  python " + path
    #command = command.replace('\n', '')
    return command

def run_shell_command(command):
    last_stdout_line = ""
    proc = None
    print(command)
    try:
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (output, errors) = proc.communicate()
        if proc.returncode:
            raise RuntimeError('error running command: %s. Return Code: %d,%s' % (command,proc.returncode,errors))
        try:
            last_stdout_line = output.decode("utf-8").split("\n")[-2]
            print("last stdoutline: " + json.dumps(last_stdout_line))
            print("script ran well")
        except Exception as ex1:
            print("error" + str(ex1))  
    except Exception as ex2:
        print("an error occured" + str(ex2))
        raise
    finally:
        try:
            if proc and hasattr(proc, 'pid'):
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        except Exception as ex3:
            print("No process found" + str(ex3))
    return last_stdout_line

def check_data(**kwargs):
    command = python_venv_command(check_data_path)
    result_string = run_shell_command(command)
    return result_string

def clean_enquiry(**kwargs):
    command = python_venv_command(clean_enquiry_path)
    result_string = run_shell_command(command)
    return result_string

def clean_retail(**kwargs):
    command = python_venv_command(clean_retail_path)
    result_string = run_shell_command(command)
    return result_string

def merge_data(**kwargs):
    command = python_venv_command(merge_data_path)
    result_string = run_shell_command(command)
    return result_string

def predict(**kwargs):
    command = python_venv_command(predict_path)
    result_string = run_shell_command(command)
    return result_string

def upload_clean_retail(**kwargs):
    command = python_venv_command(upload_cleaned_retail_path)
    result_string = run_shell_command(command)
    return result_string

def upload_clean_enquiry(**kwargs):
    command = python_venv_command(upload_cleaned_enquiry_path)
    result_string = run_shell_command(command)
    return result_string

def upload_merged_data(**kwargs):
    command = python_venv_command(upload_merged_data_path)
    result_string = run_shell_command(command)
    return result_string

def upload_predict_data(**kwargs):
    command = python_venv_command(upload_predict_path)
    result_string = run_shell_command(command)
    return result_string


check_data_columns = PythonOperator(task_id = 'check_data_columns',
                        python_callable= check_data,
                        dag = dag)

clean_enquiry_data = PythonOperator(task_id = 'clean_enquiry_data',
                        python_callable= clean_enquiry,
                        dag = dag)

clean_retail_data  = PythonOperator(task_id = 'clean_retail_data',
                        python_callable= clean_retail,
                        dag = dag)

merge_and_save_data =  PythonOperator(task_id = 'merge_and_save_data',
                        python_callable= merge_data,
                        dag = dag)

predict_results =  PythonOperator(task_id = 'predict_results',
                        python_callable= predict,
                        dag = dag)

upload_clean_retail_data =  PythonOperator(task_id = 'upload_clean_retail_data',
                        python_callable= upload_clean_retail,
                        dag = dag)

upload_clean_enquiry_data =  PythonOperator(task_id = 'upload_clean_enquiry_data',
                        python_callable= upload_clean_enquiry,
                        dag = dag)

upload_merge_data =  PythonOperator(task_id = 'upload_merge_data',
                        python_callable= upload_merged_data,
                        dag = dag)

upload_predicted_data =  PythonOperator(task_id = 'upload_predicted_data',
                        python_callable= upload_predict_data,
                        dag = dag)




check_data_columns >> clean_enquiry_data >> upload_clean_enquiry_data
check_data_columns >> clean_enquiry_data >> merge_and_save_data >> predict_results >> upload_predicted_data
check_data_columns >> clean_enquiry_data >> merge_and_save_data >> upload_merge_data
check_data_columns >> clean_retail_data >> merge_and_save_data >> predict_results >> upload_predicted_data
check_data_columns >> clean_retail_data >> merge_and_save_data >> upload_merge_data
check_data_columns >> clean_retail_data >> upload_clean_retail_data