from FileCheck import FileCheck
from CleanData import Clean
from MergeData import Merge
from DivideData import Divide
from TrainData import Train
from PredictData import Predict
from CustomerData import Customer
import pandas as pd


finance_df = pd.read_csv(r"D:\MathCo\Capstone Project\GitLab\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 9\Original_data\Finance_data\20201012_Finance_data.csv")
retail_df = pd.read_csv(r"D:\MathCo\Capstone Project\GitLab\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 9\Original_data\Retail_data\20201012_Retail_data.csv")
enquiry_df = pd.read_csv(r"D:\MathCo\Capstone Project\GitLab\bajaj_finserv_capstone\Test Scripts\Main Project Scripts\Version 9\Original_data\Enquiry_data\20201012_Enquiry_data.csv")


check = FileCheck()
print(check.check_finance_data(finance_df))
print(check.check_retail_data(retail_df))
print(check.check_enquiry_data(enquiry_df))


clean = Clean()
finance_df_cleaned = clean.clean_finance_data(finance_df)
print(finance_df_cleaned.shape)
retail_df_cleaned = clean.clean_retail_data(retail_df)
print(retail_df_cleaned.shape)
enquiry_df_cleaned = clean.clean_enquiry_data(enquiry_df)
print(enquiry_df_cleaned.shape)


merge = Merge()
retail_enquiry_df = merge.merge_retail_enquiry(retail_df_cleaned, enquiry_df_cleaned)
print(retail_enquiry_df.shape)
retail_enquiry_finance_df = merge.merge_finance(retail_df_cleaned, enquiry_df_cleaned, finance_df_cleaned)
print(retail_enquiry_finance_df.shape)


divide = Divide()
main_df, new_df = divide.divide_data(retail_enquiry_finance_df)
print(main_df.shape, new_df.shape)



print("Data SUmmary")
print(retail_enquiry_finance_df.describe(include='all').head(2))
# retail_enquiry_finance_df.describe(include='all').to_csv("Data_summary.csv")
# print(retail_enquiry_finance_df['City'].value_counts())
# print(retail_enquiry_finance_df['State'].value_counts())
# print(retail_enquiry_finance_df['ModelFamily'].value_counts())
print(retail_enquiry_finance_df['LeadType'].value_counts())
print(retail_enquiry_finance_df['LoanTaken'].value_counts())
# print(retail_enquiry_finance_df.info())
# temp = retail_enquiry_finance_df.copy()
# temp['Week/Year'] = temp['InvoiceDate'].apply(lambda x: "%d/%d" % (x.week, x.year))
# temp['Month/Year'] = temp['InvoiceDate'].apply(lambda x: "%d/%d" % (x.month, x.year))
# print(temp.groupby(['Week/Year']).size())
# print(temp.groupby(['Week/Year']).size().mean())
# print(temp.groupby(['Month/Year']).size())
# print(temp.groupby(['Month/Year']).size().mean())

train = Train()
model, performance_dict, confusion_matrix = train.train_model(main_df, new_df)
print("Yo")
print(performance_dict)
print(confusion_matrix)


# predict = Predict()
# print(predict.predict_data())


customer = Customer()
# customer.customer_data(retail_enquiry_df,model,Invoice_No="VSI100471800031")
customer.customer_data(retail_enquiry_df,model,Customer_Code="CUS-1004747680")