'''Imports'''

# import required dataframes
import json
import pandas as pd
import numpy as np
from datetime import datetime

# import undersampling libraries
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import NeighbourhoodCleaningRule
from imblearn.under_sampling import RandomUnderSampler


# import oversampling libraries
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.over_sampling import ADASYN
from imblearn.pipeline import Pipeline

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

# import ML models
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import make_pipeline, make_union
from sklearn.tree import DecisionTreeClassifier
from tpot.builtins import StackingEstimator
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold

# import model evaluation library
from sklearn.metrics import SCORERS

# import model saving library
import pickle

# class to train the data
class Train:

    # init function will set the file name to use for quality checking
    def __init__(self):
                
        # read the init.json file to extract files
        try:
            with open('init.json') as init_file:
                self.init = json.load(init_file)
            self.error_log_path = self.init['error_log']['path']
            self.classification_cols = self.init['columns']['classification']
            self.label_encode_cols = self.init['columns']['label_encode']
            self.standardize_cols = self.init['columns']['standardize']
            self.Tier1 = self.init['tiers']['tier1']
            self.Tier2 = self.init['tiers']['tier2']
            self.train_df = self.read_data()
        except FileNotFoundError:
            print("The init.json file does not exist, please check the project directory and try again")
        except KeyError:
            print("The passed init argument is improper, please check the code and rectify it")
        except Exception as e:
            print("The code stopped because of an error. The error is \n",e)
        
# ----------------------------------------------------------------------------------------------------------------------

    # function to read data
    def read_data(self):
        # read the main data
        # check if a file exists, and read if it does
        try:
            self.main_data_dir = self.init['divided_data']['main_df']['path']
            self.main_filename = sorted(os.listdir(self.main_data_dir))[-1]
            self.main_file_date = pd.to_datetime(self.main_filename.split('_')[0])
            self.main_df = pd.read_csv(self.main_data_dir+self.main_filename)
            return self.main_df
        except IndexError:
            e = "There is no file in the main data directory, please check the folder and run the code again"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="predict_data")
            return False
        except KeyError:
            e = "The folder/file name for main_df that you have passed is wrong, please check the code and try again"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="predict_data")
            return False
        except ValueError:
            e = "The date in the filename is not properly formatted, fix it and try again. Format = yyyymmdd_filename.csv"
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="predict_data")
        except Exception as e:
            a = "\tLocation: Reading Merged data.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="predict_data")
            return False

# ----------------------------------------------------------------------------------------------------------------------


    # function to convert the city names to their respective tiers
    def city_tiers(self,city):
        try:
            self.city = str(city).title()
            if self.city in self.Tier1: return "Tier1"
            elif self.city in self.Tier2: return "Tier2"
            else: return 'Tier3'
        except Exception as e:
            a = "\tLocation: City to tier conversion.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            # self.log_errors(error=e,function_name="city_tiers")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to label encode columns
    def label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.le_df = df.copy()
            # create a dict to store all the fitted encoders
            self.label_encoder_dict = dict()
            # label encode the required columns
            for col in self.label_encode_cols:
                self.label_encoder = LabelEncoder()
                self.label_encoder_dict[col] = self.label_encoder.fit(self.le_df[col])
                self.le_df[col] = self.label_encoder_dict[col].transform(self.le_df[col])
            # return the label encoded dataframe
            return self.le_df
        except Exception as e:
            a = "\tLocation: Label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse label encode columns
    def inv_label_encode_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_le_df = df.copy()
            # inverse label encode the required columns
            for col in self.label_encode_cols:
                self.inv_le_df[col] = self.label_encoder_dict[col].inverse_transform(self.inv_le_df[col].astype(int))
            # return the label encoded dataframe
            return self.inv_le_df
        except Exception as e:
            a = "\tLocation: Inverse label encoding columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_label_encode_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to standardize columns
    def standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.stan_df = df.copy()
            # create a dict to store all the fitted encoders
            self.standardize_encoder_dict = dict()
            # standardize the required columns
            for col in self.standardize_cols:
                self.mmscaler = MinMaxScaler()
                self.standardize_encoder_dict[col] = self.mmscaler.fit(self.stan_df[col].values.reshape(-1, 1))
                self.stan_df[col] = self.standardize_encoder_dict[col].transform(self.stan_df[col].values.reshape(-1, 1))
            # return the standardized dataframe
            return self.stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="standardize_df")
            return False


# ----------------------------------------------------------------------------------------------------------------------

    # function to inverse standardize columns
    def inv_standardize_df(self,df):
        try:
            # make a copy of the dataframe to work on
            self.inv_stan_df = df.copy()
            # inverse standardize the required columns
            for col in self.standardize_cols:
                self.inv_stan_df[col] = self.standardize_encoder_dict[col].inverse_transform(self.inv_stan_df[col].values.reshape(-1,1))
            # return the label encoded dataframe
            return self.inv_stan_df
        except Exception as e:
            a = "\tLocation: Using minmax scaler to inverse standardize columns.\n"
            b = "\tProblem: "
            e = a+b+str(e)
            self.log_errors(error=e,function_name="inv_standardize_df")
            return False

# ----------------------------------------------------------------------------------------------------------------------

    # function to preprocess the data
    def process_data(self, df):
        # make a copy of the dataframe to work on it
        self.process_df = df.copy()
        self.

# ----------------------------------------------------------------------------------------------------------------------

    # check if finance_df is proper
    def log_errors(self, error, function_name, class_name="Predict", script_name="PredictData.py"):
        with open(self.error_log_path,'a+') as error_log_file:
            self.now = datetime.now()
            self.dt_string = self.now.strftime("%Y/%m/%d_%H:%M:%S")
            error_log_file.write("Time: {}\n".format(self.dt_string))
            error_log_file.write("Script name: {}\n".format(script_name))
            error_log_file.write("Class name: {}\n".format(class_name))
            error_log_file.write("Function name: {}\n".format(function_name))
            error_log_file.write("Error: \n{}\n\n".format(error))
