# import the necessary libraries
import pandas as pd
import numpy as np

# function to extract numerical pincodes
def extract_pin(x):
    try:
        x = float(x)
    except:
        x = np.nan
    return x

# function to extract valid pincodes
def get_valid_pin(x):
    try:
        x = float(str(x)[:6])
    except:
        x = np.nan
    if x < 100:
        x = np.nan
    return x

# function to clean pincode_df
def clean_pincode_df(pincode_df):

    # preprocess the pincode dataframe
    pincode_df = pincode_df[['pincode','Districtname','statename']]
    pincode_df.columns = ['Pin','City','State']
    pincode_df['City'] = pincode_df['City'].str.title()
    pincode_df['State'] = pincode_df['State'].str.title()
    pincode_df['Pin'] = pincode_df['Pin'].apply(extract_pin).apply(get_valid_pin)
    pincode_df = pincode_df.groupby('Pin').first()
    pincode_df = pincode_df.reset_index(level=0)

    # return the cleaned pincode_df
    return pincode_df

pincode_df = pd.read_csv(r'D:\MathCo\Capstone Project\GitLab\bajaj_finserv_capstone\Daily_work\Original Data\pin_sheet.csv')
pincode_df = clean_pincode_df(pincode_df)
pincode_df.to_csv("pincode_df.csv",index=False)
