def custom_one_hot_encoding(df, OHE_out_cols):
    OHE_out_df = pd.DataFrame(np.zeros((len(df), len(OHE_out_cols))), columns=OHE_out_cols)    
    try: OHE_out_df['Segment_M1'] = np.where(df['Segment']=='M1',1,0)
    except: pass
    try: OHE_out_df['Segment_M2'] = np.where(df['Segment']=='M2',1,0)
    except: pass
    try: OHE_out_df['Segment_M3'] = np.where(df['Segment']=='M3',1,0)
    except: pass
    try: OHE_out_df['Segment_S1'] = np.where(df['Segment']=='S1',1,0)
    except: pass
    try: OHE_out_df['Segment_S2'] = np.where(df['Segment']=='S2',1,0)
    except: pass
    try: OHE_out_df['LeadType_Walk In'] = np.where(df['LeadType']=='Walk In',1,0)
    except: pass
    try: OHE_out_df['LeadType_No Enquiry'] = np.where(df['LeadType']=='No Enquiry',1,0)
    except: pass
    try: OHE_out_df['LeadType_Telephone'] = np.where(df['LeadType']=='Telephone',1,0)
    except: pass
    try: OHE_out_df['LeadType_Activity/Outdoor'] = np.where(df['LeadType']=='Activity/Outdoor',1,0)
    except: pass
    try: OHE_out_df['LeadType_Others'] = np.where(df['LeadType']=='Others',1,0)
    except: pass
    try: OHE_out_df['TestRideOffered_No'] = np.where(df['TestRideOffered']=='No',1,0)
    except: pass
    try: OHE_out_df['TestRideOffered_No Enquiry'] = np.where(df['TestRideOffered']=='No Enquiry',1,0)
    except: pass
    try: OHE_out_df['TestRideOffered_Test Ride Taken'] = np.where(df['TestRideOffered']=='Test Ride Taken',1,0)
    except: pass
    try: OHE_out_df['TestRideOffered_Customer Declined'] = np.where(df['TestRideOffered']=='Customer Declined',1,0)
    except: pass
    try: OHE_out_df['FollowUp_1st Follow up'] = np.where(df['FollowUp']=='1st Follow up',1,0)
    except: pass
    try: OHE_out_df['FollowUp_2nd Follow up'] = np.where(df['FollowUp']=='2nd Follow up',1,0)
    except: pass
    try: OHE_out_df['FollowUp_3rd Follow up'] = np.where(df['FollowUp']=='3rd Follow up',1,0)
    except: pass
    try: OHE_out_df['FollowUp_General Follow up'] = np.where(df['FollowUp']=='General Follow up',1,0)
    except: pass
    try: OHE_out_df['FollowUp_No Enquiry'] = np.where(df['FollowUp']=='No Enquiry',1,0)
    except: pass
    try: OHE_out_df['SourceOfEnquiry_Source Not Available'] = np.where(df['SourceOfEnquiry']=='Source Not Available',1,0)
    except: pass
    try: OHE_out_df['SourceOfEnquiry_No Enquiry'] = np.where(df['SourceOfEnquiry']=='No Enquiry',1,0)
    except: pass
    try: OHE_out_df['SourceOfEnquiry_General Enquiry'] = np.where(df['SourceOfEnquiry']=='General Enquiry',1,0)
    except: pass
    try: OHE_out_df['SourceOfEnquiry_Others'] = np.where(df['SourceOfEnquiry']=='Others',1,0)
    except: pass
    try: OHE_out_df['SourceOfEnquiry_Referral'] = np.where(df['SourceOfEnquiry']=='Referral',1,0)
    except: pass
    try: OHE_out_df['BranchType_Urban'] = np.where(df['BranchType']=='Urban',1,0)
    except: pass
    try: OHE_out_df['BranchType_Rural'] = np.where(df['BranchType']=='Rural',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Apache'] = np.where(df['ModelFamily']=='Apache',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Splendor'] = np.where(df['ModelFamily']=='Splendor',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Star city'] = np.where(df['ModelFamily']=='Star city',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Intruder'] = np.where(df['ModelFamily']=='Intruder',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_CBZ'] = np.where(df['ModelFamily']=='CBZ',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Discover'] = np.where(df['ModelFamily']=='Discover',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_RE'] = np.where(df['ModelFamily']=='RE',1,0)
    except: pass
    try: OHE_out_df['ModelFamily_Boxer'] = np.where(df['ModelFamily']=='Boxer',1,0)
    except: pass
    try: OHE_out_df['City_Tier1'] = np.where(df['City']=='Tier1',1,0)
    except: pass
    try: OHE_out_df['City_Tier2'] = np.where(df['City']=='Tier2',1,0)
    except: pass
    try: OHE_out_df['City_Tier3'] = np.where(df['City']=='Tier3',1,0)
    except: pass
    try: OHE_out_df['DealerCity_Tier1'] = np.where(df['DealerCity']=='Tier1',1,0)
    except: pass
    try: OHE_out_df['DealerCity_Tier2'] = np.where(df['DealerCity']=='Tier2',1,0)
    except: pass
    try: OHE_out_df['DealerCity_Tier3'] = np.where(df['DealerCity']=='Tier3',1,0)
    except: pass
    try: OHE_out_df['State_Southern'] = np.where(df['State']=='Southern',1,0)
    except: pass
    try: OHE_out_df['State_Northern'] = np.where(df['State']=='Northern',1,0)
    except: pass
    try: OHE_out_df['State_Western'] = np.where(df['State']=='Western',1,0)
    except: pass
    try: OHE_out_df['State_Eastern'] = np.where(df['State']=='Eastern',1,0)
    except: pass
    try: OHE_out_df['State_Central'] = np.where(df['State']=='Central',1,0)
    except: pass
    try: OHE_out_df['State_Northeastern'] = np.where(df['State']=='Northeastern',1,0)
    except: pass
    try: OHE_out_df['State_Arabian_Sea'] = np.where(df['State']=='Arabian_Sea',1,0)
    except: pass
    try: OHE_out_df['State_Bay_Of_Bengal'] = np.where(df['State']=='Bay_Of_Bengal',1,0)
    except: pass
    return OHE_out_df