# imort required libraries
import pandas as pd
import numpy as np
import json
import pickle
import os

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

# import model evaluation library
from sklearn.metrics import SCORERS


# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)


'''Initializations - Variables'''

# create initial variables
count = 0
invoice_city_dict = dict()
invoice_dlcity_dict = dict()
encoder_dict = dict()


'''Initialization - Lists'''

classification_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'TotalAmount', 'City', 'State', 'DealerCity', 'ModelFamily']
label_encode_cols = ['Segment', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
standardize_cols = ['TotalAmount']
# List down Tier 1 and 2 companies
Tier1 = ['Bengaluru','Chennai','Delhi','Hyderabad','Kolkata','Mumbai','Ahmedabad','Pune']
Tier2 = ['Agra','Aligarh','Amravati','Asansol','Bareilly','Bhavnagar','Bhopal','Bikaner','Bokaro Steel City',
         'Coimbatore','Dehradun','Bhilai','Erode','Firozabad','Gorakhpur','Guntur','Gurgaon','Hubli','Dharwad',
         'Indore','Jaipur','Jammu','Jamshedpur','Jodhpur','Kannur','Kochi','Kolhapur','Kota','Kurnool',
         'Lucknow','Malappuram','Goa','Meerut','Mysore','Nanded','Nellore','Palakkad','Perinthalmanna','Purulia',
         'Rajkot','Ranchi','Salem','Shimla','Solapur','Thiruvananthapuram','Tiruchirappalli','Tirupati','Tiruppur',
         'Ujjain','Vadodara','Vasai','Virar City','Vellore','Surat','Ajmer','Allahabad','Amritsar','Aurangabad',
         'Belgaum','Bhiwandi','Bhubaneswar','Bilaspur','Chandigarh','Cuttack','Dhanbad','Durgapur','Faridabad',
         'Ghaziabad','Gulbarga','Gwalior','Guwahati','Hamirpur','Jabalpur','Jalandhar','Jamnagar','Jhansi','Kakinada',
         'Kanpur','Kottayam','Kollam','Kozhikode','Ludhiana','Madurai','Mathura','Mangalore','Moradabad','Nagpur',
         'Nashik','Noida','Patna','Pondicherry','Raipur','Rajahmundry','Rourkela','Sangli','Siliguri','Srinagar',
         'Thrissur','Tirur','Tirunelveli','Tiruvannamalai','Bijapur','Varanasi','Vijayawada','Warangal','Visakhapatnam']



'''Function definitions'''

# function to create invoice-city pair dictionary
def make_invoice_city_dict(df,city_col="City"):
    # create a dict of invoice number and city
    inv_df = df.copy()
    inv_df = inv_df.set_index('InvoiceNo')
    invoice_city_dict = inv_df.to_dict()[city_col]    
    
    # return the dictionary
    return invoice_city_dict


# function to convert the city names to their respective tiers
def city_tiers(x):
    x = x.title()
    if x in Tier1: return "Tier1"
    elif x in Tier2: return "Tier2"
    else: return 'Tier3'


"""Main prediction function"""

# function to predict the output
def predict():
    
    # read the new data to predict
    df = pd.read_csv(init['divided_data']["new_df"]['path'])


    # read the performance metrics data
    performance_metrics_df = pd.read_csv(init['performance_data']["performance_metrics_df"]['path'])
    print('hey')

    # create a dict of invoice number and city
    invoice_city_dict = make_invoice_city_dict(df,city_col='City')
    invoice_dlcity_dict = make_invoice_city_dict(df,city_col='DealerCity')
    
    
    # convert cities into tiers
    df['City'] = df['City'].apply(city_tiers)
    df['DealerCity'] = df['DealerCity'].apply(city_tiers)
    
    
    # label encode the required columns
    for col in label_encode_cols:
        label_encoder = LabelEncoder()
        encoder_dict[col] = label_encoder.fit(df[col])
        df[col] = encoder_dict[col].transform(df[col])
       
    
    # standardize the required columns
    for col in standardize_cols:
        mmscaler = MinMaxScaler()
        encoder_dict[col] = mmscaler.fit(df[col].values.reshape(-1, 1))
        df[col] = encoder_dict[col].transform(df[col].values.reshape(-1, 1))


    # load the model
    filename = performance_metrics_df['filename'][0]
    model_path = init['all_models']['path']
    model = pickle.load(open(model_path+filename, 'rb'))


    # Set X and y for testing
    y = df['LoanTaken']
    X = df.drop('LoanTaken',axis=1)


    # calculate performance metrics for the model
    accuracy = SCORERS['accuracy'](model, X[classification_cols], y)
    precision = SCORERS['precision'](model, X[classification_cols], y)
    recall = SCORERS['recall'](model, X[classification_cols], y)
    f1_score = SCORERS['f1'](model, X[classification_cols], y)



    # predict the final outpput list
    ypredict = model.predict(X[classification_cols])
    ypredict_prob = model.predict_proba(X[classification_cols])[:,1]
    
    
    # convert numpy arrays to pandas series
    ypredict = pd.Series(ypredict)
    ypredict_prob = pd.Series(ypredict_prob)
    

    # combine the final prediction with dataframe
    df_without_loan_col = df.reset_index().iloc[:,1:].drop('LoanTaken',axis=1)
    predicted_df = pd.concat([df_without_loan_col,ypredict,ypredict_prob],axis=1)
    predicted_df.columns = list(df_without_loan_col.columns) + ['LoanTaken_predict','LoanTaken_probability']
    
    
    # inverse label encode the columns
    for col in label_encode_cols:
        predicted_df[col] = encoder_dict[col].inverse_transform(predicted_df[col].astype(int))
        
    
    # inverse standardize the columns
    for col in standardize_cols:
        predicted_df[col] = encoder_dict[col].inverse_transform(predicted_df[col].values.reshape(-1,1))
        
        
    # convert cities to their original names from tiers
    predicted_df['City'] = predicted_df['InvoiceNo'].map(invoice_city_dict)
    predicted_df['DealerCity'] = predicted_df['InvoiceNo'].map(invoice_dlcity_dict)


    # calculate feature importance of each feature
    feats = {} # a dict to hold feature_name: feature_importance
    for feature, importance in zip(classification_cols, model.feature_importances_):
        feats[feature] = importance 

    # sort the sictionary with respect to features importance value
    feats = {column: importance for column, importance in sorted(feats.items(), key=lambda item: item[1], reverse=True)}

    # convert decimal score to percentage for feature importance
    for feature, importance in feats.items():
        feats[feature] = str(round(importance*100,2))+"%"
        
    # log the performance scores
    prediction_log_path = init['prediction_log']['path']
    with open(prediction_log_path,'w+') as prediction_log_file:
        prediction_log_file.write("Model Name: {}\n".format(type(model).__name__))
        prediction_log_file.write("Model Parameters:\n")
        json.dump(model.get_params(),prediction_log_file,indent=2)
        prediction_log_file.write("\n\n")
        prediction_log_file.write("Performance Metrics:\n")
        prediction_log_file.write("Accuracy: {}\n".format(accuracy))
        prediction_log_file.write("Precision: {}\n".format(precision))
        prediction_log_file.write("Recall: {}\n".format(recall))
        prediction_log_file.write("F1_Score: {}\n\n".format(f1_score))
        prediction_log_file.write("Features and their contribution to prediction:\n")
        json.dump(feats,prediction_log_file,indent=2)
    
    
    # save the predicted dataframe
    predicted_df.to_csv(init['prediction_data']["predicted_df"]['path'],index=False)

    