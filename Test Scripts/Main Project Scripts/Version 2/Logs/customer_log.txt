Customer information:
{
  "Enquiry": "ENQ105902016002552",
  "State": "Chattisgarh",
  "ModelCode": "00DU11",
  "ChassisNo": "XY2A18AZ5GWF08823",
  "InvoiceDate": "2017-03-31",
  "Color": "FLAME RED DECAL RED",
  "Segment": "M1",
  "Enquiry_flag": 1,
  "Booking_flag": 1,
  "CustomerCode": "CUS-105907866",
  "InvoiceNo": "VSI105902016001755_1",
  "LeadType": "No Enquiry",
  "TestRideOffered": "No Enquiry",
  "FollowUp": "No Enquiry",
  "SourceOfEnquiry": "No Enquiry",
  "BranchType": "Urban",
  "City": "Bilaspur(Cgh)",
  "DealerCity": "Bilaspur",
  "TotalAmount": 38107.0,
  "ModelFamily": "Splendor",
  "LoanTaken_predict": 0,
  "LoanTaken_probability": 0.32269933443856985
}

Top 5 factors driving the customer:
{
  "FollowUp": "55.59%",
  "State": "23.52%",
  "TestRideOffered": "20.51%",
  "TotalAmount": "18.01%",
  "ModelFamily": "5.98%"
}