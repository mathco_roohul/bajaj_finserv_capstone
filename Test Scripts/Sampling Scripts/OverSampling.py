from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
from imblearn.pipeline import Pipeline
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.over_sampling import SVMSMOTE
from imblearn.over_sampling import ADASYN


# Individual oversamplers

# SMOTE: Synthetic Minority Oversampling TEchnique
def SM(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    oversample = SMOTE()
    # transform the dataset
    X, y = oversample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['SM'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Borderline SMOTE
def BSM(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    oversample = BorderlineSMOTE()
    # transform the dataset
    X, y = oversample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['BSM'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# SVM SMOTE
def SVMSM(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    oversample = SVMSMOTE()
    # transform the dataset
    X, y = oversample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['SVMSM'] = (X,y)
    # return dict of transformed datasets
    return out_dict

def ASYSM(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    oversample = ADASYN()
    # transform the dataset
    X, y = oversample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['ASYSM'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Oversampling + Undersampling Pipeline

# Pipeline: SMOTE + RandomUnderSampler
def SM_RUS(X,y):
    # define output dictionary
    out_dict = dict()
    # define pipeline
    over = SMOTE(sampling_strategy=0.5)
    under = RandomUnderSampler(sampling_strategy=0.5)
    steps = [('o', over), ('u', under)]
    pipeline = Pipeline(steps=steps)
    # transform the dataset
    X, y = pipeline.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['SM_RUS'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# Pipeline: SMOTE + RandomUnderSampler
def BSM_RUS(X,y):
    # define output dictionary
    out_dict = dict()
    # define pipeline
    over = BorderlineSMOTE(sampling_strategy=0.5)
    under = RandomUnderSampler(sampling_strategy=0.5)
    steps = [('o', over), ('u', under)]
    pipeline = Pipeline(steps=steps)
    # transform the dataset
    X, y = pipeline.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['BSM_RUS'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# Pipeline: SMOTE + RandomUnderSampler
def SVMSM_RUS(X,y):
    # define output dictionary
    out_dict = dict()
    # define pipeline
    over = SVMSMOTE(sampling_strategy=0.5)
    under = RandomUnderSampler(sampling_strategy=0.5)
    steps = [('o', over), ('u', under)]
    pipeline = Pipeline(steps=steps)
    # transform the dataset
    X, y = pipeline.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['SVMSM_RUS'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# Pipeline: SMOTE + RandomUnderSampler
def ASYSM_RUS(X,y):
    # define output dictionary
    out_dict = dict()
    # define pipeline
    over = ADASYN(sampling_strategy=0.5)
    under = RandomUnderSampler(sampling_strategy=0.5)
    steps = [('o', over), ('u', under)]
    pipeline = Pipeline(steps=steps)
    # transform the dataset
    X, y = pipeline.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['ASYSM_RUS'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Multiple sampled outputs

# Combination of individual methods
def IN_SM(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['SM'] = SM(X,y)['SM']
    out_dict['BSM'] = BSM(X,y)['BSM']
    out_dict['SVMSM'] = SVMSM(X,y)['SVMSM']
    out_dict['ASYSM'] = ASYSM(X,y)['ASYSM']
    # return dict of transformed datasets
    return out_dict

# Combination of pipelines
def PIPE_SM(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['SM_RUS'] = SM_RUS(X,y)['SM_RUS']
    out_dict['BSM_RUS'] = BSM_RUS(X,y)['BSM_RUS']
    out_dict['SVMSM_RUS'] = SVMSM_RUS(X,y)['SVMSM_RUS']
    out_dict['ASYSM_RUS'] = ASYSM_RUS(X,y)['ASYSM_RUS']
    # return dict of transformed datasets
    return out_dict

# All oversampling methods
def ALL(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['SM'] = SM(X,y)['SM']
    out_dict['BSM'] = BSM(X,y)['BSM']
    out_dict['SVMSM'] = SVMSM(X,y)['SVMSM']
    out_dict['ASYSM'] = ASYSM(X,y)['ASYSM']
    out_dict['SM_RUS'] = SM_RUS(X,y)['SM_RUS']
    out_dict['BSM_RUS'] = BSM_RUS(X,y)['BSM_RUS']
    out_dict['SVMSM_RUS'] = SVMSM_RUS(X,y)['SVMSM_RUS']
    out_dict['ASYSM_RUS'] = ASYSM_RUS(X,y)['ASYSM_RUS']
    # return dict of transformed datasets
    return out_dict


# Final Function to run any function
def OverSample(X,y,sampling_method='SM'):
    """
    This function will oversample the give data based on the sampling method chosen by the user and returns a dictionary as output


    X = Feature variables of the dataset

    y = Target variable of thr dataset

    sampling_method = The sampling method of choice
        default = SM

        Different types of sampling methods are 

        a. Individual methods:
            SM: SMOTE (Synthetic Minority Oversampling TEchnique)
            BSM: Boderline SMOTE
            SVMSM: SVM SMOTE
            ASYSM: Adaptive Synthetic Sampling

        b. Pipeline methods:
            SM_RUS: SMOTE + Random Undersampling
            BSM_RUS: Boderline SMOTE + Random Undersampling
            SVMSM_RUS: SVM SMOTE + Random Undersampling
            ASYSM_RUS: Adaptive Synthetic Sampling + Random Undersampling

        c. Combination methods:
            In_SM: All individual methods
            PIPE_SM: All pipeline methods
            ALL: ALL methods
    
    source: https://machinelearningmastery.com/smote-oversampling-for-imbalanced-classification/
    """

    if sampling_method == 'SM':
        return SM(X,y)
    elif sampling_method == 'BSM':
        return BSM(X,y)
    elif sampling_method == 'SVMSM':
        return SVMSM(X,y)
    elif sampling_method == 'ASYSM':
        return ASYSM(X,y)
    elif sampling_method == 'SM_RUS':
        return SM_RUS(X,y)
    elif sampling_method == 'BSM_RUS':
        return BSM_RUS(X,y)
    elif sampling_method == 'SVMSM_RUS':
        return SVMSM_RUS(X,y)
    elif sampling_method == 'ASYSM_RUS':
        return ASYSM_RUS(X,y)
    elif sampling_method == 'IN_SM':
        return IN_SM(X,y)
    elif sampling_method == 'PIPE_SM':
        return PIPE_SM(X,y)
    elif sampling_method == 'ALL':
        return ALL(X,y)
