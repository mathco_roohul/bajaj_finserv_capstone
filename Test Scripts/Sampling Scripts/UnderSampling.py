from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import CondensedNearestNeighbour
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import NeighbourhoodCleaningRule


# Methods that Select Examples to Keep

# NearMiss
def NM(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = NearMiss(version=3)
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['NM'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# CondensedNearestNeighbour
def CNN(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = CondensedNearestNeighbour(n_neighbors=1)
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['CNN'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Methods that Select Examples to Delete

# TomekLinks
def TL(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = TomekLinks()
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['TL'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# EditedNearestNeighbours
def ENN(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = EditedNearestNeighbours()
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['ENN'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Keep and Delete Methods

# OneSideSelection
def OSS(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = OneSidedSelection()
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['OSS'] = (X,y)
    # return dict of transformed datasets
    return out_dict

# NeighbourhoodCleaningRule
def NCR(X,y):
    # define output dictionary
    out_dict = dict()
    # define the undersampling method
    undersample = NeighbourhoodCleaningRule(n_neighbors=3, threshold_cleaning=0.5)
    # transform the dataset
    X, y = undersample.fit_resample(X, y)
    # add outputs of samplers into dict
    out_dict['NCR'] = (X,y)
    # return dict of transformed datasets
    return out_dict


# Multiple sampled outputs

# Combination of "Select Examples to keep" methods
def SEK(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['NM'] = NM(X,y)['NM']
    out_dict['CNN'] = CNN(X,y)['CNN']
    # return dict of transformed datasets
    return out_dict

# Combination of "Select Examples to delete" methods
def SED(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['TL'] = TL(X,y)['TL']
    out_dict['ENN'] = ENN(X,y)['ENN']
    # return dict of transformed datasets
    return out_dict

# Combination of "Keep and Delete" methods
def KAD(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['OSS'] = OSS(X,y)['OSS']
    out_dict['NCR'] = NCR(X,y)['NCR']
    # return dict of transformed datasets
    return out_dict

# Combination of all methods
def ALL(X,y):
    # define output dictionary
    out_dict = dict()
    # add outputs of samplers into dict
    out_dict['NM'] = NM(X,y)['NM']
    out_dict['CNN'] = CNN(X,y)['CNN']
    out_dict['TL'] = TL(X,y)['TL']
    out_dict['ENN'] = ENN(X,y)['ENN']
    out_dict['OSS'] = OSS(X,y)['OSS']
    out_dict['NCR'] = NCR(X,y)['NCR']
    # return dict of transformed datasets
    return out_dict


# Final Function to run any function
def UnderSample(X,y,sampling_method='NM'):
    """
    This function will undersample the give data based on the sampling method chosen by the user and returns a dictionary as output


    X = Feature variables of the dataset

    y = Target variable of thr dataset

    sampling_method = The sampling method of choice
        default = NM

        Different types of sampling methods are 

        a. Individual methods:
            NM: Near Miss 
            CNN: Condensed Nearest Neighbour
            TL: Tomek Links
            ENN: Edited Nearest Neighbours
            OSS: One Side Selection
            NCR: Neighbourhood Cleaning Rule

        b. Combination methods:
            SEK: Combination of NM and CNN
            SED: Combination of TL and ENN
            KAD: Combination of OSS and NCR
            ALL: COmbination of all methods
    
    source: https://machinelearningmastery.com/undersampling-algorithms-for-imbalanced-classification/
    """


    if sampling_method == 'NM':
        return NM(X,y)
    elif sampling_method == 'CNN':
        return CNN(X,y)
    elif sampling_method == 'TL':
        return TL(X,y)
    elif sampling_method == 'ENN':
        return ENN(X,y)
    elif sampling_method == 'OSS':
        return OSS(X,y)
    elif sampling_method == 'NCR':
        return NCR(X,y)
    elif sampling_method == 'SEK':
        return SEK(X,y)
    elif sampling_method == 'SED':
        return SED(X,y)
    elif sampling_method == 'KAD':
        return KAD(X,y)
    elif sampling_method == 'ALL':
        return ALL(X,y)