from BlobConnect import Blob
import io
import pandas as pd


blob = Blob()

print(blob.delete_data("Sample_data/Enquiry_data/20201012_Enquiry_data_sample.csv"))
print(blob.delete_data("Sample_data/Retail_data/20201012_Retail_data_sample.csv"))

output = io.StringIO()
df = pd.read_csv("Enquiry1.csv")
output = df.to_csv(index_label="idx", encoding = "utf-8", index=False)
print(blob.upload_data("Sample_data/Enquiry_data/20201012_Enquiry_data_sample.csv",output))

output = io.StringIO()
df = pd.read_csv("Retail1.csv")
output = df.to_csv(index_label="idx", encoding = "utf-8", index=False)
print(blob.upload_data("Sample_data/Retail_data/20201012_Retail_data_sample.csv",output))

print(blob.download_data("Sample_data/Retail_data/20201012_Retail_data_sample.csv"))


blob2 = Blob()