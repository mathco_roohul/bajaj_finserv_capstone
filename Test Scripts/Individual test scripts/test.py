from BlobConnect import Blob
import io
import pandas as pd

output = io.StringIO()
blob = Blob()

df = blob.download_data("Finance_data.csv")
print(df)
print(type(df))
output = df.to_csv(index_label="idx", encoding = "utf-8", index=False)
print(output)
print(type(output))
print(blob.upload_data("Finance_data2.csv",output))

blob2 = Blob()