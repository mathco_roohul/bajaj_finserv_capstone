# import required libraries
import pandas as pd
import numpy as np
import json
import pickle
import os
import eli5

# import data preprocessing libraries
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler

# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)


'''Initializations - Variables'''

# create initial variables
count = 0
invoice_city_dict = dict()
invoice_dlcity_dict = dict()
encoder_dict = dict()


'''Initialization - Lists'''

classification_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'TotalAmount', 'City', 'State', 'DealerCity', 'ModelFamily']
no_classification_cols = ['Enquiry','ModelCode','ChassisNo','InvoiceDate','Color','CustomerCode','InvoiceNo']
categorical_cols = ['Segment', 'Enquiry_flag', 'Booking_flag', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
label_encode_cols = ['Segment', 'LeadType', 'TestRideOffered', 'FollowUp', 'SourceOfEnquiry', 'BranchType', 'City', 'State', 'DealerCity', 'ModelFamily']
standardize_cols = ['TotalAmount']
# List down Tier 1 and 2 companies
Tier1 = ['Bengaluru','Chennai','Delhi','Hyderabad','Kolkata','Mumbai','Ahmedabad','Pune']
Tier2 = ['Agra','Aligarh','Amravati','Asansol','Bareilly','Bhavnagar','Bhopal','Bikaner','Bokaro Steel City',
         'Coimbatore','Dehradun','Bhilai','Erode','Firozabad','Gorakhpur','Guntur','Gurgaon','Hubli','Dharwad',
         'Indore','Jaipur','Jammu','Jamshedpur','Jodhpur','Kannur','Kochi','Kolhapur','Kota','Kurnool',
         'Lucknow','Malappuram','Goa','Meerut','Mysore','Nanded','Nellore','Palakkad','Perinthalmanna','Purulia',
         'Rajkot','Ranchi','Salem','Shimla','Solapur','Thiruvananthapuram','Tiruchirappalli','Tirupati','Tiruppur',
         'Ujjain','Vadodara','Vasai','Virar City','Vellore','Surat','Ajmer','Allahabad','Amritsar','Aurangabad',
         'Belgaum','Bhiwandi','Bhubaneswar','Bilaspur','Chandigarh','Cuttack','Dhanbad','Durgapur','Faridabad',
         'Ghaziabad','Gulbarga','Gwalior','Guwahati','Hamirpur','Jabalpur','Jalandhar','Jamnagar','Jhansi','Kakinada',
         'Kanpur','Kottayam','Kollam','Kozhikode','Ludhiana','Madurai','Mathura','Mangalore','Moradabad','Nagpur',
         'Nashik','Noida','Patna','Pondicherry','Raipur','Rajahmundry','Rourkela','Sangli','Siliguri','Srinagar',
         'Thrissur','Tirur','Tirunelveli','Tiruvannamalai','Bijapur','Varanasi','Vijayawada','Warangal','Visakhapatnam']


        
'''Function definitions'''

# function to create invoice-city pair dictionary
def make_invoice_city_dict(df,city_col="City"):
    # create a dict of invoice number and city
    inv_df = df.copy()
    inv_df = inv_df.set_index('InvoiceNo')
    invoice_city_dict = inv_df.to_dict()[city_col]    
    
    # return the dictionary
    return invoice_city_dict


# function to convert the city names to their respective tiers
def city_tiers(x):
    x = x.title()
    if x in Tier1: return "Tier1"
    elif x in Tier2: return "Tier2"
    else: return 'Tier3'



"""Main customer function"""

# function to get customer data
def get_customer_info(Invoice_No):

    # read the predicted df
    df = pd.read_csv(init["prediction_data"]["predicted_df"]["path"])

    # read the performance metrics data
    performance_metrics_df = pd.read_csv(init['performance_data']["performance_metrics_df"]['path'])

    # load the model
    filename = performance_metrics_df['filename'][0]
    model_path = init['all_models']['path']
    model = pickle.load(open(model_path+filename, 'rb'))

    # Get columns used for classification
    non_labelled_df = df[df.columns[~df.columns.isin(no_classification_cols)]]
    labelled_df = non_labelled_df.copy()

    # convert cities into tiers
    labelled_df['City'] = labelled_df['City'].apply(city_tiers)
    labelled_df['DealerCity'] = labelled_df['DealerCity'].apply(city_tiers)

    # label encode the required columns
    for col in label_encode_cols:
        label_encoder = LabelEncoder()
        labelled_df[col] = label_encoder.fit_transform(labelled_df[col])

    # standardize the required columns
    for col in standardize_cols:
        mmscaler = MinMaxScaler()
        labelled_df[col] = mmscaler.fit_transform(labelled_df[col].values.reshape(-1, 1))

    # select df with classification columns to explain prediction 
    explain_row = df[df['InvoiceNo']==Invoice_No].index

    # explain the prediction
    model_explanation = eli5.explain_prediction_df(model, labelled_df.iloc[explain_row][classification_cols], feature_names = classification_cols) 

    # drop the bias row
    index_to_drop = model_explanation[model_explanation['feature']=='<BIAS>'].index
    model_explanation = model_explanation.drop(index=index_to_drop)[['feature','weight']]

    # calculate the contribution ofeach feature
    weights = model_explanation['weight'].values
    contribution = weights/weights.sum(axis=0,keepdims=1)
    model_explanation['contribution'] = contribution


    # customer_data
    customer_row = df.iloc[explain_row].T.to_dict()[explain_row[0]]

    # top 5 features
    model_explanation = model_explanation.set_index('feature').head()
    top_5_features = model_explanation.to_dict()['contribution']
    for feature, contribution in top_5_features.items():
        top_5_features[feature] = str(round(top_5_features[feature]*100,2))+"%"
    print(customer_row)
    print(top_5_features)


    # log the customer information
    customer_log_path = init['customer_log']['path']
    with open(customer_log_path,'w+') as customer_log_file:
        customer_log_file.write("Customer information:\n")
        json.dump(customer_row,customer_log_file,indent=2)
        customer_log_file.write("\n\n")
        customer_log_file.write("Top 5 factors driving the customer:\n")
        json.dump(top_5_features,customer_log_file,indent=2)