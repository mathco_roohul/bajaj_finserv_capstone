# import required dataframes
import json
import pandas as pd
import numpy as np

# read paths of the data files
with open('init.json') as init_file:
    init = json.load(init_file)

# function to extract numerical pincodes
def extract_pin(x):
    try:
        x = float(x)
    except:
        x = np.nan
    return x

# function to extract valid pincodes
def get_valid_pin(x):
    try:
        x = float(str(x)[:6])
    except:
        x = np.nan
    if x < 100:
        x = np.nan
    return x

# function to clean InvoiceDate column
def clean_dates(date):
    try:
        date = pd.to_datetime(date)
        if date.date() == pd.to_datetime('today').date():
            date = np.nan
    except:
        date = np.nan
    return date

# clean TotalAmount column
def clean_total_amount(df, high_quantile=0.90, low_quantile=0.01):
    # Get a list of medians for all model families
    price_median_dict = df.groupby('ModelFamily')['TotalAmount'].median().to_dict()

    # Extract 90% quantile value for each model family
    price_high_quantile_dict = dict()
    for model in price_median_dict.keys():
        price_high_quantile_dict[model] = df[df['ModelFamily']==model]['TotalAmount'].quantile(high_quantile)
        
    # Extract 1% quantile value for each model family
    price_low_quantile_dict = dict()
    for model in price_median_dict.keys():
        price_low_quantile_dict[model] = df[df['ModelFamily']==model]['TotalAmount'].quantile(low_quantile)

    # Change all upper quantile values to median
    for model in price_median_dict.keys():
        df.loc[(df['ModelFamily']==model) & 
                (df['TotalAmount'] > price_high_quantile_dict[model]),'TotalAmount'] = price_median_dict[model]

    # Change all lower quantile values to median
    for model in price_median_dict.keys():
        df.loc[(df['ModelFamily']==model) & 
                (df['TotalAmount'] < price_low_quantile_dict[model]),'TotalAmount'] = price_median_dict[model]

    return df

# function to clean the dataset
def clean(filename='enquiry_df'):

    # select the required columns
    columns = init["original_data"][filename]["columns"]
    
    # read the data
    df = pd.read_csv(init["original_data"][filename]["path"])
    df = df[columns]

    # drop duplicate rows
    df = df.drop_duplicates()

    # change column name in enquiry from DocName to Enquiry
    if filename == "enquiry_df":
        df = df.rename(columns={"DocName": "Enquiry"})

    # clean Enquiry column from enquiry_df
    if "Enquiry" in df.columns:
        df['Enquiry'] = np.where(df["Enquiry"].str.isalnum(),df['Enquiry'],np.nan)

    # convert cochin to kochi in retail and enquiry datasets
    if "DealerCity" in df.columns:
        df['DealerCity'] = df['DealerCity'].str.replace("COCHIN","KOCHI").tolist()
    
    # change column name in retail from DealerState to State
    if filename == "retail_df":
        df = df.rename(columns={"DealerState": "State"})
    
    # clean the pincode column
    if "Pin" in df.columns:
        df['Pin'] = df['Pin'].apply(extract_pin).apply(get_valid_pin)
    
    # clean city and state names in retail and enquiry datasets
    if "City" in df.columns and "State" in df.columns:
        pincode_df = pd.read_csv(init["supplimentary_data"]["pincode_df"]["path"])
        pincode_df = pincode_df.set_index('Pin')
        pin_city_dict = pincode_df.to_dict()['City']
        pin_state_dict = pincode_df.to_dict()['State']
        df['City'] = df['Pin'].map(pin_city_dict)
        df['State'] = df['Pin'].map(pin_state_dict)
        df = df.drop('Pin', axis=1)

    # convert City, DealerCity, State to title case
    if "City" in df.columns and "DealerCity" in df.columns and "State" in df.columns:
        df['City'] = df['City'].str.title()
        df['DealerCity'] = df['DealerCity'].str.title()
        df['State'] = df['State'].str.title()

    # clean branch type column in retail and finance datasets
    if "BranchType" in df.columns:
        city_branch_df = pd.read_csv(init["supplimentary_data"]["city_branch_df"]["path"])
        city_branch_df = city_branch_df.set_index("DealerCity")
        city_branch_dict = city_branch_df.to_dict()['BranchType']
        df['BranchType'] = df['DealerCity'].map(city_branch_dict)

    # clean TotalAmount column from retail and enquiry datasets
    if "TotalAmount" in df.columns:
        df = clean_total_amount(df)

    # clean TestRideOffered column from enquiry_df
    if "TestRideOffered" in df.columns:
        df['TestRideOffered'] = df['TestRideOffered'].fillna("No")

    # clean SourceOfEnquiry column from enquiry_df
    if "SourceOfEnquiry" in df.columns:
        df['SourceOfEnquiry'] = df['SourceOfEnquiry'].fillna("Source Not Available")

    # clean ChassisNo column from retail and finance datasets
    if "ChassisNo" in df.columns:
        df['ChassisNo'] = df['ChassisNo'].astype(str)
        df = df[df['ChassisNo'].map(len) == 17]
    
    # clean Segment column from retail_df
    if "Segment" in df.columns:
        df = df[df['Segment'].isin(['M1','M2','M3','S1','S2'])]
    
    # make Enquiry flag and Booking flag in retail_df
    if filename == "retail_df":
        df['Enquiry_flag'] = np.where(df['Enquiry'].isna(),0,1)
        df['Booking_flag'] = np.where(df['BookingNo_x'].isna(),0,1)
        df = df.drop("BookingNo_x",axis=1)
    
    # change column name in Finance_df from MAKE to ModelFamily
    if filename == "finance_df":
        df = df.rename(columns={"MAKE": "ModelFamily"})
        df = df.rename(columns={"AMTFIN": "LoanAmount"})
        df = df.rename(columns={"CUSTOMERID": "CustomerID"})

    # Clean InvoiceDate column
    if "InvoiceDate" in df.columns:
        df['InvoiceDate'] = df['InvoiceDate'].apply(clean_dates)

    # drop all rows with null values
    df = df.dropna(axis=0)

    # drop duplicate rows once more
    df = df.drop_duplicates()
    
    # save the dataframe
    # return df
    df.to_csv(init['cleaned_data'][filename]['path'],index=False)
    